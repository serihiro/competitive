#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int k;

void dfs(vector<int> v, int size) {
  if (size == k) {
    for (int const &i : v) {
      cout << i << ",";
    }
    cout << endl;
    return;
  }

  v.push_back(0);
  while (v.back() <= 9) {
    dfs(v, size + 1);
    ++v.back();
  }
}

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  cin >> k;
  dfs(vector<int>(), 0);

  return 0;
}
