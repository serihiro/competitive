#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int A, B;
  cin >> A >> B;

  if (A < B) {
    cout << B + max(B - 1, A) << endl;
  } else {
    cout << A + max(A - 1, B) << endl;
  }

  return 0;
}
