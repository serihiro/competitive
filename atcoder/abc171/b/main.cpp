#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N, K;
  cin >> N >> K;
  vector<int> P(N);

  REP(i, N) { cin >> P.at(i); }
  sort(ALL(P));
  int sum = 0;
  REP(i, K) { sum += P.at(i); }

  cout << sum << endl;

  return 0;
}
