#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  ll A, B, C;
  cin >> A >> B >> C;

  ll ans = 0;
  while (true) {
    if (A % 2 != 0 || B % 2 != 0 || C % 2 != 0) {
      cout << ans << endl;
      return 0;
    }

    if (A == B && B == C) {
      cout << "-1" << endl;
      return 0;
    }

    ll a = A / 2;
    ll b = B / 2;
    ll c = C / 2;
    A = b + c;
    B = a + c;
    C = a + b;

    ++ans;
  }

  return 0;
}
