#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int B[20];

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N, M, C;
  cin >> N >> M >> C;
  REP(i, M) { cin >> B[i]; }

  int ans = 0;
  int sum = 0;

  int tmp_a = 0;

  REP(i, N) {
    sum = 0;
    REP(j, M) {
      cin >> tmp_a;
      sum += B[j] * tmp_a;
    }
    if (sum + C > 0) {
      ++ans;
    }
  }

  cout << ans << endl;

  return 0;
}
