#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  vector<pair<int, pair<int, int>>> S;
  REP(i, 10) { S.push_back(make_pair(i, make_pair(i + 1, i + 2))); }

  REP(i, 10) {
    cout << S[i].first << ":" << S[i].second.first << ":" << S[i].second.second
         << endl;
    // cout << S.at(i).first << ":" << S.at(i).second.first << ":"
    //      << S.at(i).second.second;
  }
}
