#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  string S;
  cin >> N >> S;
  int l = (int)S.length();
  REP(i, l) { cout << (char)((S.at(i) - 'A' + N) % 26 + 'A'); }
  cout << endl;

  return 0;
}
