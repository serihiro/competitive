#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;
  vector<int> S(N);
  int sum = 0;
  REP(i, N) {
    cin >> S.at(i);
    sum += S.at(i);
  }

  if (sum % 10 != 0) {
    cout << sum << endl;
    return 0;
  }

  sort(S.begin(), S.end());
  bool found = false;
  REP(i, N) {
    if (S.at(i) % 10 == 0) {
      continue;
    }
    sum -= S.at(i);
    found = true;
    break;
  }

  if (found) {
    cout << sum << endl;
  } else {
    cout << 0 << endl;
  }

  return 0;
}
