/*
1 20 8 160
1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
*/

#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  ll H, W, K, V;
  ll A[125][125];

  cin >> H >> W >> K >> V;
  REP(i, H) {
    REP(j, W) { cin >> A[i][j]; }
  }

  // Supportes only one dimentional A
  ll total;
  ll result = -1;

  if (H == 1 && W == 1) {
    if (A[0][0] + K <= V) {
      cout << 1 << endl;
    } else {
      cout << 0 << endl;
    }

    return 0;
  }

  for (ll i = W; i > 0; --i) {
    for (ll j = 0; j < W - (i - 1); ++j) {
      total = 0;
      for (ll k = j; k < j + i; ++k) {
        // cout << i << ":" << j << ":" << k << endl;
        total += A[0][k];
      }

      if (total + i * K <= V) {
        result = max(i, result);
        // cout << total + i * K << ":" << result << endl;
      }
    }
  }

  if (result == -1) {
    cout << 0 << endl;
  } else {
    cout << result << endl;
  }

  return 0;
}
