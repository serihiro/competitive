#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  int N;
  cin >> N;
  int F = 0, K = 0;
  REP(i, N) {
    int a;
    cin >> a;
    if (a % 4 == 0)
      F++;
    if (a % 2 == 1)
      K++;
  }

  if (N % 2) {
    cout << (K - 1 <= F ? "Yes" : "No") << endl;
  } else {
    cout << (K <= F ? "Yes" : "No") << endl;
  }

  return 0;
}
