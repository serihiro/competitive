#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;

  const ll MOD = 1e9 + 7;
  ll ans = 1;
  REP(i, N) {
    ans *= (i + 1);
    ans %= MOD;
  }

  cout << ans % MOD << endl;

  return 0;
}
