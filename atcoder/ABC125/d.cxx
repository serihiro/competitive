#include <iostream>
#include <algorithm>

using namespace std;
typedef long long ll;

ll N;
ll B[100000];

int main()
{
    cin.tie(0);
    ios::sync_with_stdio(false);

    cin >> N;
    ll result = 0;
    for (ll i = 0; i < N; ++i)
    {
        cin >> B[i];
        if(i > 0){
            if(-1 * B[i-1] + -1 * B[i] > B[i-1] + B[i]){
               B[i-1] = -1 * B[i-1];
               B[i] = -1 * B[i];
            }
        }
    }

    for(ll i = 0; i < N; ++i){
        result += B[i];
    }

    cout << result << endl;

    return 0;
}
