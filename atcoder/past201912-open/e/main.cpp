#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N, Q;
  cin >> N >> Q;
  vector<pair<int, pair<int, int>>> S;

  REP(i, Q) {
    int k;
    int a;
    int b;

    cin >> k >> a;
    if (k == 1) {
      cin >> b;
      S.push_back(make_pair(k, make_pair(a, b)));
    } else {
      S.push_back(make_pair(k, make_pair(a, 0)));
    }
  }

  vector<set<int>> Fs(N + 1);
  vector<set<int>> Fd(N + 1);

  REP(i, Q) {
    int k = S.at(i).first;
    pair<int, int> c = S.at(i).second;

    if (k == 1) {
      Fs.at(c.first).insert(c.second);
      Fd.at(c.second).insert(c.first);
    } else if (k == 2) {
      auto fd = Fd.at(c.first);
      for (int j : fd) {
        Fs.at(c.first).insert(j);
        Fd.at(j).insert(c.first);
      }
    } else {
      auto fs = Fs.at(c.first);
      for (int j : fs) {
        for (int k : Fs.at(j)) {
          Fs.at(c.first).insert(k);
          Fd.at(k).insert(c.first);
        }
      }
      Fs.at(c.first).erase(c.first);
      Fd.at(c.first).erase(c.first);
    }
  }

  REPI(i, 1, N + 1) {
    REPI(j, 1, N + 1) {
      if (Fs.at(i).find(j) != Fs.at(i).end()) {
        cout << "Y";
      } else {
        cout << "N";
      }
    }
    cout << endl;
  }

  return 0;
}
