#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N, M;
  cin >> N >> M;

  int l = 1;
  int r = N;
  if (N % 2 == 1) {
    REP(i, M) {
      cout << l << " " << r << endl;
      ++l;
      --r;
    }
  } else {
    set<pair<int, int>> found;

    REP(i, M) {
      cout << l << " " << r << endl;
      ++l;
      --r;
      if (abs(r - l) == abs(l + N - r) ||
          found.find(make_pair(min(abs(r - l), abs(l + N - r)),
                               max(abs(r - l), abs(l + N - r)))) !=
              found.end()) {
        ++l;
      }
      found.insert(make_pair(min(abs(r - l), abs(l + N - r)),
                             max(abs(r - l), abs(l + N - r))));
    }
  }

  return 0;
}
