#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  ll N, X;
  cin >> N >> X;
  vector<ll> x(N + 1);
  REP(i, N) { cin >> x.at(i); }
  x.at(N) = X;
  sort(x.begin(), x.end());
  vector<ll> y(N);
  REP(i, N) { y.at(i) = x.at(i + 1) - x.at(i); }
  ll min_diff = *min_element(y.begin(), y.end());
  ll ans = min_diff;
  REP(i, N) {
    if (y.at(i) % min_diff != 0) {
      ans = __gcd(ans, y.at(i));
    }
  }

  cout << ans << endl;

  return 0;
}
