#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N, K;
  cin >> N >> K;
  int ans = 0;
  int x;
  REP(i, N) {
    cin >> x;
    ans += min(x * 2, abs(K - x) * 2);
  }

  cout << ans << endl;

  return 0;
}
