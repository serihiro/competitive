#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
using namespace std;

void solve() {
  int n;
  cin >> n;
  vector<int> A(n);
  vector<vector<int>> X(n, vector<int>(n));
  vector<vector<int>> Y(n, vector<int>(n));

  REP(i, n) {
    cin >> A[i];
    REP(j, A[i]) {
      cin >> X[i][j] >> Y[i][j];
      X[i][j]--;
    }
  }

  int ans = 0;
  bool ok = true;
  REP(bits, (1 << n)) {
    ok = true;
    REP(i, n) {
      if (!(bits & (1 << i)))
        continue;
      REP(j, A[i]) {
        if (bits & (1 << X[i][j]))
          ok &= Y[i][j];
        else
          ok &= !Y[i][j];
      }
    }
    if (ok)
      ans = max(ans, (int)__builtin_popcount(bits));
  }
  cout << ans << endl;
}

signed main() {
  ios_base::sync_with_stdio(false);
  cin.tie(0);
  cout.tie(0);

  solve();
  return 0;
}
