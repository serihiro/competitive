#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int A[1000];

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  float avg = 0.0;
  cin >> N;
  REP(i, N) {
    cin >> A[i];
    avg += A[i];
  }
  avg = floor(avg / N);
  REP(i, N) { cout << abs(avg - A[i]) << endl; }

  return 0;
}
