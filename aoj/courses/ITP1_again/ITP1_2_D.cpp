#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int W, H, x, y, r;
  cin >> W >> H >> x >> y >> r;

  if (y + r > H) {
    cout << "No" << endl;
    return 0;
  } else if (x + r > W) {
    cout << "No" << endl;
    return 0;
  } else if (y - r < 0) {
    cout << "No" << endl;
    return 0;
  } else if (x - r < 0) {
    cout << "No" << endl;
    return 0;
  }

  cout << "Yes" << endl;

  return 0;
}
