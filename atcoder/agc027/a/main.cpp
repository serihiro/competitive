#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  ll x;
  cin >> N >> x;
  vector<int> a;
  int _a;
  REP(i, N) {
    cin >> _a;
    a.push_back(_a);
  }
  sort(a.begin(), a.end());
  if (accumulate(a.begin(), a.end(), 0ll) == x) {
    cout << N << endl;
    return 0;
  }

  int ans = 0;
  int sum = 0;
  REP(i, N) {
    sum += a.at(i);
    if (sum > x) {
      break;
    }
    ++ans;
  }

  cout << min(N - 1, ans) << endl;

  return 0;
}
