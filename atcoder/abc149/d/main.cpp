#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N, K, R, S, P;
  string T, U;

  cin >> N >> K;
  cin >> R >> S >> P;
  cin >> T;
  U = "";

  ll ans = 0;

  REP(i, N) {
    if (i < K) {
      if (T[i] == 'r') {
        ans += P;
        U.push_back('p');
      } else if (T[i] == 's') {
        ans += R;
        U.push_back('r');
      } else if (T[i] == 'p') {
        ans += S;
        U.push_back('s');
      }
    } else {
      if (T[i] == 'r') {
        ans += P;
        U.push_back('p');
      } else if (T[i] == 's') {
        ans += R;
        U.push_back('r');
      } else if (T[i] == 'p') {
        ans += S;
        U.push_back('s');
      }
    }
  }

  cout << ans << endl;

  return 0;
}
