#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

long long const MOD = 1000000007;

ll fact(int n) {
  if (n == 1) {
    return 1;
  } else {
    return n * fact(n - 1);
  }
}

ll modinv(ll a, ll m) {
  ll b = m;
  ll u = 1;
  ll v = 0;
  ll t = a / b;

  while (b) {
    t = a / b;
    a -= t * b;
    swap(a, b);
    u -= t * v;
    swap(u, v);
  }
  u %= m;
  if (u < 0)
    u += m;

  return u;
}

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;
  vector<ll> x(N, 0ll);
  REP(i, N) { cin >> x[i]; }
  long long ans = 0;
  REP(i, N - 1) {
    // ans = ans + ((x[N - 1] - x[i]) % MOD) * modinv(N - 1, MOD) % MOD;
    ans = ans + (x[N - 1] - x[i]) % MOD;
    cout << ans << endl;
  }
  ans = ans * modinv(N - 1, MOD) % MOD;
  ans = ans * fact(N - 1) % MOD;
  cout << ans << endl;

  return 0;
}
