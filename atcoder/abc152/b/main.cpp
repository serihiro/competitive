#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int a, b;
  string c = "", d = "";
  cin >> a >> b;

  REP(i, a) { c += to_string(b); }
  REP(i, b) { d += to_string(a); }

  if (a >= b) {
    cout << c << endl;
  } else {
    cout << d << endl;
  }

  return 0;
}
