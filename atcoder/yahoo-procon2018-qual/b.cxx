#include <iostream>
#include <string>
#include <cmath>
using namespace std;

#define REP(i, n) for (int i = 0; i < (n); ++i)

int main()
{
    cin.tie(0);
    ios::sync_with_stdio(false);

    double x = 0;
    int k = 0;

    cin >> x >> k;

    if (k == 0)
    {
        cout << x + 1 << endl;
        return 0;
    }
    else
    {
        if (log10(x) + 1)
        {
            double new_x = floor(x / pow(10, k));
            cout << new_x + 1;
            REP(i, k)
            {
                cout << "0";
            }
            cout << endl;
        }
        else
        {
            cout << "1";
            REP(i, k)
            {
                cout << "0";
            }
            cout << endl;
        }
    }

    return 0;
}
