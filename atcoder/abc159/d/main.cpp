#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

ll c2(ll v) {
  if (v == 2ll) {
    return 1ll;
  } else if (v <= 1) {
    return 0ll;
  }
  return v * (v - 1ll) / 2ll;
}

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  ll N;
  cin >> N;
  unordered_map<ll, ll> count;
  vector<ll> A(N);
  ll a;
  REP(i, N) {
    cin >> a;
    if (count.find(a) == count.end()) {
      count.insert(make_pair(a, 1));
    } else {
      ++count.at(a);
    }
    A.at(i) = a;
  }

  ll total = 0;
  for (auto const it : count) {
    if (it.second >= 2) {
      total += c2(it.second);
    }
  }

  REP(k, N) {
    ll a2 = A.at(k);
    ll ans = total - c2(count.at(a2));
    ans += c2(count.at(a2) - 1);

    cout << ans << endl;
  }

  return 0;
}
