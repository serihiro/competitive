// giveup!!!!
// https://yahoo-procon2018-qual.contest.atcoder.jp/tasks/yahoo_procon2018_qual_c

#include <iostream>
#include <string>
#include <string>
#include <sstream>
#include <vector>
using namespace std;

#define REP(i, n) for (int i = 0; i < (n); ++i)

std::vector<std::string> split(const std::string &input, char delimiter)
{
    std::istringstream stream(input);

    std::string field;
    std::vector<std::string> result;
    while (std::getline(stream, field, delimiter))
    {
        result.push_back(field);
    }
    return result;
}

int main()
{
    cin.tie(0);
    ios::sync_with_stdio(false);

    int n = 0;
    long long X[18] = {};
    long long C[18] = {};
    long long V[18] = {};

    string tmp_l = "";
    long long tmp_v = 0;

    cin >> n;

    cin.ignore();
    getline(cin, tmp_l);
    int i = 0;
    for (const string &s : split(tmp_l, ' '))
    {
        X[i] = atoi(s.c_str());
        ++i;
    }

    cin.ignore();
    getline(cin, tmp_l);
    i = 0;
    for (const string &s : split(tmp_l, ' '))
    {
        C[i] = atoi(s.c_str());
        ++i;
    }

    cin.ignore();
    getline(cin, tmp_l);
    i = 0;
    for (const string &s : split(tmp_l, ' '))
    {
        V[i] = atoi(s.c_str());
        ++i;
    }

    return 0;
}
