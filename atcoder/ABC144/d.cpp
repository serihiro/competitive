#include <bits/stdc++.h>
#include <cmath>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()
#define rad_to_deg(rad) (((rad) / 2 / M_PI) * 360)

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  double a, b, x, hight;

  cin >> a >> b >> x;
  hight = x / (a * a);

  cout << a << " " << b << " " << hight << endl;

  double current_degree = std::atan2(hight, a) * (180.0 / M_PI);
  double boundary_degree = std::atan2(b, a) * (180.0 / M_PI);

  cout << current_degree << ":" << boundary_degree << endl;

  cout << (90 - boundary_degree) - current_degree << endl;

  return 0;
}
