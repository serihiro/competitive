#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N, K;
  cin >> N >> K;

  unordered_map<int, int> count;
  int a;
  REP(i, N) {
    cin >> a;
    if (count.find(a) == count.end()) {
      count.insert(make_pair(a, 1));
    } else {
      ++count.at(a);
    }
  }

  multimap<int, int> r_count;
  for (auto const it : count) {
    r_count.insert(make_pair(it.second, it.first));
  }

  int ans = 0;
  int v_size = count.size();
  for (auto const it : r_count) {
    if (v_size <= K) {
      break;
    }
    ans += it.first;
    --v_size;
  }

  cout << ans << endl;

  return 0;
}
