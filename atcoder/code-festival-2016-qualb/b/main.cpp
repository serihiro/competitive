#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N, A, B;
  cin >> N >> A >> B;
  string S;
  cin >> S;

  int cur = 0;
  int ab_cur = 0;
  REP(i, N) {
    string ans = "No";
    if (S.at(i) == 'a') {
      if (cur < (A + B)) {
        ++cur;
        ans = "Yes";
      }
    } else if (S.at(i) == 'b') {
      if (cur < (A + B) && ab_cur < B) {
        ++cur;
        ++ab_cur;
        ans = "Yes";
      }
    }

    cout << ans << endl;
  }

  return 0;
}
