#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
typedef long double ld;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  long double L, V_S, D_S;
  long double X[2019];
  long double V[2019];
  long double D[2019];

  cin >> N >> L;
  cin >> V_S >> D_S;
  REP(i, N) {
    cin >> X[i];
    cin >> V[i];
    cin >> D[i];
  }

  if (N == 0) {
    if (L <= D_S) {
      cout << setprecision(10) << L / V_S << endl;
    } else {
      cout << "impossible" << endl;
    }

    return 0;
  }

  if (N == 1) {
    // 乗り換えても乗り換えなくてもいい
    if (L <= D_S) {
      if (L <= X[0] + D[0]) {
        cout << setprecision(10)
             << min((L / V_S), ((X[0] / V_S) + (L - X[0]) / V[0])) << endl;
      } else {
        cout << setprecision(10) << L / V_S << endl;
      }
      // 乗り換え必須
    } else if (X[0] <= D_S && L <= X[0] + D[0]) {
      cout << setprecision(10) << X[0] / V_S + (L - X[0]) / V[0] << endl;
      // どうやっても無理
    } else {
      cout << "impossible" << endl;
    }

    return 0;
  }

  return 0;
}
