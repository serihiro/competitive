// #include <bits/stdc++.h>
// #include <numeric>


// #define REP(i, x) for (int i = 0; i < (int)(x); i++)
// #define ALL(x) (x).begin(), (x).end()

// typedef long long ll;
// using namespace std;

// int main() {
//   cin.tie(0);
//   ios::sync_with_stdio(false);
  
//   ll A, B;
//   cin >> A >> B;

//   vector<int> candidates;

//   for(int i=1; i < max(sqrt(A), sqrt(B)); ++i){
//     if((A % i == 0) && (B % i == 0)){
//       candidates.push_back(i);
//     }
//   }

//   ll result = 0;
//   for(int i=0; i < candidates.size(); ++i){
//     for(int j = i+1; j<candidates.size(); ++j){
//       cout << candidates[i] << ":" << candidates[j] << ":" << __gcd(candidates[i], candidates[j])  << endl;

//       if(__gcd(candidates[i], candidates[j]) == 1){
//         ++result;
//       }
//     }
//   }

//   cout << result << endl;

//   return 0;
// }


#include<iostream>
#include<cmath>
#include<vector>
long long gcd(long long a,long long b){
    while(a && b){
        if(a > b)a%=b;
        else b%=a;
    }
    return a+b;
}

int main(){
    long long a,b;
    long long ans=0; 
    std::cin>>a>>b;

    const long long C = gcd(a,b);
    long long c = C;

    for(int i=2;i<std::sqrt(C);i++){
        if(c%i==0){
            ans++;
            while(c%i==0)c/=i;
        }
    }
    if(c!=1)ans++;
    std::cout<<ans+1<<std::endl;
}
