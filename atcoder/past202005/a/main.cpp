#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  string s, t;
  cin >> s >> t;
  if (s == t) {
    cout << "same" << endl;
    return 0;
  }

  for (char &c : s) {
    if ('A' <= c && c <= 'Z') {
      c = (char)(c + 32);
    }
  }

  for (char &c : t) {
    if ('A' <= c && c <= 'Z') {
      c = (char)(c + 32);
    }
  }

  if (s == t) {
    cout << "case-insensitive" << endl;
  } else {
    cout << "different" << endl;
  }

  return 0;
}
