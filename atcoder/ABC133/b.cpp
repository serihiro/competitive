#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int X[10][10];

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N, D;

  cin >> N >> D;
  REP(i, N) {
    REP(j, D) { cin >> X[i][j]; }
  }

  int ans = 0;
  float d1 = 0;
  float d2 = 0;
  for (int i = 0; i < N; ++i) {
    for (int j = i + 1; j < N; ++j) {
      d1 = 0;
      for (int k = 0; k < D; ++k) {
        d1 += (abs(X[i][k] - X[j][k]) * abs(X[i][k] - X[j][k]));
      }
      d2 = sqrt(d1);
      if (d2 == ceil(d2)) {
        ans++;
      }
    }
  }

  cout << ans << endl;

  return 0;
}
