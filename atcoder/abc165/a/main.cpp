#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int K, A, B;
  cin >> K >> A >> B;

  for (int i = A; i <= B; ++i) {
    if (i % K == 0) {
      cout << "OK" << endl;
      return 0;
    }
  }

  cout << "NG" << endl;

  return 0;
}
