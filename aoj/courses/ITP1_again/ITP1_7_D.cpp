#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int n, m, l;
  cin >> n >> m >> l;

  vector<vector<ll>> a(n, vector<ll>(m));
  vector<vector<ll>> b(m, vector<ll>(l));
  vector<vector<ll>> c(n, vector<ll>(l, 0ll));
  REP(i, n) {
    REP(j, m) { cin >> a.at(i).at(j); }
  }
  REP(j, m) {
    REP(k, l) { cin >> b.at(j).at(k); }
  }

  REP(i, n) {
    REP(j, m) {
      REP(k, l) { c.at(i).at(k) += a.at(i).at(j) * b.at(j).at(k); }
    }
  }

  REP(i, n) {
    REP(k, l) {
      cout << c.at(i).at(k);
      if (k != l - 1) {
        cout << " ";
      }
    }
    cout << endl;
  }

  return 0;
}
