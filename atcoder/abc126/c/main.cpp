#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int f(int i, int k) {
  int ans = 0;
  while (k > i) {
    i *= 2;
    ++ans;
  }
  return ans;
}

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  double N, K;
  cin >> N >> K;
  double p1 = 1 / N;
  double ans = 0.0;
  REPI(i, 1, N + 1) { ans += p1 * pow(0.5, f(i, K)); }

  cout << setprecision(9) << ans << endl;

  return 0;
}
