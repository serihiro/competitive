#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int X;
  cin >> X;
  int ans = 1;
  REPI(i, 1, X) {
    REPI(j, 2, X) {
      if (pow(i, j) <= X) {
        ans = max(ans, (int)pow(i, j));
      } else {
        break;
      }
    }
  }

  cout << ans << endl;

  return 0;
}
