#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;
  vector<int> A(N);
  REP(i, N) { cin >> A.at(i); }
  int min_a = *min_element(A.begin(), A.end());
  int max_a = *max_element(A.begin(), A.end());
  int ans = 1 << 29;
  int cur = 0;
  REPI(i, min_a, max_a + 1) {
    cur = 0;
    REP(j, N) { cur += ((A.at(j) - i) * (A.at(j) - i)); }
    ans = min(ans, cur);
  }

  cout << ans << endl;

  return 0;
}
