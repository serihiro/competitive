#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N, X, Y, Z, a, b;


  cin >> N >> X >> Y >> Z;

  int result = 0;

  REP(i, N) {
    cin >> a >> b;
    if(X <= a && Y <= b && Z <= a + b){
      ++result;
    }
  }

  cout << result << endl;

  return 0;
}
