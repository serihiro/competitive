#include <iostream>
using namespace std;

int main()
{
    cin.tie(0);
    ios::sync_with_stdio(false);

    string S;
    cin >> S;

    long long n = S.length();
    long long ans = 1 << 29;
    for (long long i = 1; i <= n - 1; ++i)
    {
        if (S[i - 1] != S[i])
        {
            ans = min(ans, (long long)max(i, n - i));
        }
    }

    if (ans == 1 << 29)
    {
        ans = n;
    }
    cout << ans << endl;

    return 0;
}
