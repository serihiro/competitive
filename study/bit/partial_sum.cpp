/*
20
30
92
69
42
56
46
28
25
8
57
91
99
88
18
37
4
25
67
85
13
102
*/

#include <bits/stdc++.h>
using namespace std;

#define REP(i, x) for (int i = 0; i < (int)(x); i++)

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N, K;
  cin >> N;
  vector<int> a(N);
  REP(i, N) { cin >> a[i]; }
  cin >> K;

  bool ans = false;
  int sum = 0;
  for (int bit = 0; bit < (1 << N); ++bit) {
    sum = 0;
    REP(i, N) {
      if (bit & (1 << i)) {
        sum += a[i];
        if (sum > K) {
          continue;
        }
      }
    }

    if (sum == K) {
      ans = true;
      break;
    }
  }

  ans ? cout << "Yes" << endl : cout << "No" << endl;
}
