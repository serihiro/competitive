#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;

  int a;
  REP(i, N) {
    cin >> a;
    if (a % 2 == 0) {
      if (!(a % 3 == 0 || a % 5 == 0)) {
        cout << "DENIED" << endl;
        return 0;
      }
    }
  }

  cout << "APPROVED" << endl;

  return 0;
}
