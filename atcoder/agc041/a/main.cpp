#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  ll A, B, N;
  cin >> N >> A >> B;
  ll diff = B - A;
  if (diff == 0) {
    cout << 0 << endl;
    return 0;
  }

  if (A % 2 == B % 2) {
    cout << (B - A) / 2 << endl;
  } else {
    cout << min(A - 1, N - B) + 1 + (B - A - 1) / 2 << endl;
  }

  return 0;
}
