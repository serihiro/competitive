#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  ll X;
  cin >> X;

  vector<ll> sum(100010, 0);
  map<ll, ll> m;

  for (ll i = 1; i < 1e5; ++i) {
    sum.at(i) = sum.at(i - 1) + i;
    m.insert(make_pair(sum.at(i), i));
  }

  ll c = *lower_bound(sum.begin(), sum.end(), X);
  cout << m.at(c) << endl;

  return 0;
}
