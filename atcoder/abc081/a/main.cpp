#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  string s;
  cin >> s;
  int ans = 0;
  if (s.at(0) == '1')
    ++ans;
  if (s.at(1) == '1')
    ++ans;
  if (s.at(2) == '1')
    ++ans;

  cout << ans << endl;

  return 0;
}
