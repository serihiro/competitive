#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  string S;
  cin >> S;
  int l = S.length();

  int ans1 = 0;
  int ans2 = 0;

  REP(i, l) {
    if (i % 2 == 1) {
      if (S.at(i) == '0') {
        ++ans1;
      } else {
        ++ans2;
      }
    } else {
      if (S.at(i) == '1') {
        ++ans1;
      } else {
        ++ans2;
      }
    }
  }

  cout << min(ans1, ans2) << endl;

  return 0;
}
