#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N, K, tmp_h;

  int result = 0;

  cin >> N >> K;
  REP(i, N) {
    cin >> tmp_h;
    if(tmp_h >= K){
      ++result;
    }
  }

  cout << result << endl;
  
  return 0;
}
