// https://qiita.com/drken/items/3b4fdf0a78e7a138cd9a

#include <iostream>

using namespace std;

int const MOD = 1000000007;

int main() {
  int a, b, c;

  a = 111111111;
  b = 123456789;
  c = 987654321;

  cout << "a + b + c" << endl;
  cout << "wrong: " << (a + b + c) % MOD << endl;
  cout << "correct: " << (a + b % MOD) + c % MOD << endl;

  cout << "a - b - c" << endl;
  cout << "wrong: " << (a - b - c) % MOD << endl;
  cout << "wrong: " << ((a - b) % MOD - c) % MOD << endl;
  cout << "correct:" << (a - b - c) % MOD + MOD << endl;
  cout << "correct:" << (a - b % MOD) - c % MOD + MOD << endl;

  long long d, e, f;
  d = 111111111;
  e = 123456789;
  f = 987654321;

  cout << "d * e * f" << endl;
  cout << "wrong: " << d * e * f % MOD << endl;
  cout << "correct: " << (d * e % MOD) * f % MOD << endl;
  cout << "correct: " << d % MOD * e % MOD * f % MOD << endl;

  return 0;
}