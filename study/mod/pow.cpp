#include <iostream>
using namespace std;

typedef long long ll;

ll modpow(ll a, ll n, ll mod) {
  ll res = 1;
  while (n > 0) {
    if (n & 1)
      res = res * a % mod;
    a = a * a % mod;
    n >>= 1;
  }
  return res;
}
int main() { cout << modpow(2, 10, 1000000007) << endl; }