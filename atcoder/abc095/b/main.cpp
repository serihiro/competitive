#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N, X;
  cin >> N >> X;
  vector<int> M(N);
  REP(i, N) {
    cin >> M.at(i);
    X -= M.at(i);
  }
  int min_m = *min_element(M.begin(), M.end());
  int ans = N;
  while (X >= min_m) {
    X -= min_m;
    ++ans;
  }

  cout << ans << endl;

  return 0;
}
