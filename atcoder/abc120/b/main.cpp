#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int A, B, K;
  cin >> A >> B >> K;
  int k = 0;

  for (int i = 100; i >= 0; --i) {
    if (A % i == 0 && B % i == 0) {
      ++k;
    }
    if (K == k) {
      cout << i << endl;
      return 0;
    }
  }

  return 0;
}
