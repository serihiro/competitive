#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  string S, T;
  cin >> S >> T;
  map<string, int> f = {
      {"B1", 9},  {"B2", 8},  {"B3", 7},  {"B4", 6},  {"B5", 5},  {"B6", 4},
      {"B7", 3},  {"B8", 2},  {"B9", 1},  {"1F", 10}, {"2F", 11}, {"3F", 12},
      {"4F", 13}, {"5F", 14}, {"6F", 15}, {"7F", 16}, {"8F", 17}, {"9F", 18}};
  cout << abs(f.at(S) - f.at(T)) << endl;

  return 0;
}
