#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int n, x;
  while (cin >> n >> x) {
    if (n == 0 && x == 0) {
      return 0;
    }

    int ans = 0;
    REPI(i, 1, n + 1) {
      REPI(j, i + 1, n + 1) {
        if (x - i - j > n || i + j >= x) {
          continue;
        }
        if (x - i - j > j) {
          ++ans;
        }
      }
    }

    cout << ans << endl;
  }

  return 0;
}
