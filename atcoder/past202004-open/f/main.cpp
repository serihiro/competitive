#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);
  int N;
  cin >> N;
  map<int, vector<int>> T;
  int a, b;
  REP(i, N) {
    cin >> a >> b;
    --a;
    if (T.find(a) == T.end()) {
      T.insert(make_pair(a, vector<int>(1, b)));
    } else {
      T.at(a).push_back(b);
    }
  }

  int ans = 0;
  priority_queue<int> q;
  REP(i, N) {
    if (T.find(i) != T.end()) {
      for (int const v : T.at(i)) {
        q.push(v);
      }
    }

    if (!q.empty()) {
      ans += q.top();
      q.pop();
    }
    cout << ans << endl;
  }

  return 0;
}
