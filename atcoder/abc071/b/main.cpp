#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  string S;
  cin >> S;
  vector<int> found(26, 0);
  int l = S.length();
  REP(i, l) { ++found.at(S.at(i) - 'a'); }
  REP(i, 26) {
    if (found.at(i) == 0) {
      cout << char('a' + i) << endl;
      return 0;
    }
  }

  cout << "None" << endl;

  return 0;
}
