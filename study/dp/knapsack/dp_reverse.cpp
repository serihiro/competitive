// https://www.slideshare.net/iwiwi/ss-3578511 p.19
// O(NU)
// DPテーブルを上から埋めていくタイプのDP．

/*
case 1

6 8
2 1 3 2 1 5
3 2 6 1 3 85

91
*/
#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int N, U;
int W[101];
int V[101];
int dp[101][100001];

int do_dp() {
  // 初期条件
  REP(u, U) dp[0][u] = 0;

  for (int i = 0; i <= N; ++i) {
    for (int u = 0; u <= U; ++u) {
      if (u < W[i]) {
        dp[i + 1][u] = dp[i][u];
      } else {
        dp[i + 1][u] = max(dp[i][u], dp[i][u - W[i]] + V[i]);
      }
    }
  }

  REP(i, 7) {
    REP(u, 9) { cout << dp[i][u] << ", "; }
    cout << endl;
  }

  return dp[N][U];
}

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  cin >> N >> U;
  REP(i, N) { cin >> W[i]; }
  REP(i, N) { cin >> V[i]; }

  int ans = do_dp();
  cout << ans << endl;

  return 0;
}
