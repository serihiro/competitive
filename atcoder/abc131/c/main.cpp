#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

ll lcm(ll x, ll y) { return x / __gcd(x, y) * y; }

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  ll A, B, C, D;
  cin >> A >> B >> C >> D;
  ll high, low, ng;
  ng = 0ll;

  high = B >= C ? B / C : 0;
  low = A >= C ? (A + C - 1) / C : 0;
  // cout << high << ":" << low << "/" << ng << endl;
  if (high > 0) {
    if (low > 0) {
      ng += high - low + 1;
    } else {
      ng += high;
    }
  }

  high = B >= D ? B / D : 0;
  low = A >= D ? (A + D - 1) / D : 0;
  // cout << high << ":" << low << "/" << ng << endl;
  if (high > 0) {
    if (low > 0) {
      ng += high - low + 1;
    } else {
      ng += high;
    }
  }

  ll l = lcm(C, D);
  high = B >= l ? B / l : 0;
  low = A >= l ? (A + l - 1) / l : 0;
  // cout << high << ":" << low << "/" << ng << endl;
  if (high > 0) {
    if (low > 0) {
      ng -= (high - low + 1);
    } else {
      ng -= high;
    }
  }

  cout << B - A + 1 - ng << endl;

  return 0;
}
