#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  string S;
  cin >> S;

  string ans = "";
  for (const char &c : S) {
    if (c == '0') {
      ans += "0";
    } else if (c == '1') {
      ans += "1";
    } else if (c == 'B') {
      if (ans.length() > 0) {
        ans.pop_back();
      }
    }
  }

  cout << ans << endl;

  return 0;
}
