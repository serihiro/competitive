#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    cin.tie(0);
    ios::sync_with_stdio(false);

    int n = 0;
    int a = 0;
    int b = 0;

    cin >> n >> a >> b;
    int sum = 0;
    int t_sum = 0;
    int t_i = 0;

    for (int i = 1; i < n + 1; i++)
    {
        t_sum = 0;
        t_i = i;
        while (t_i > 0)
        {
            t_sum += t_i % 10;
            t_i /= floor(10);
        }
        if (a <= t_sum && t_sum <= b)
        {
            sum += i;
        }
    }

    cout << sum << endl;

    return 0;
}