#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;

  deque<ll> q;
  ll a;

  if (N % 2 == 0) {
    REPI(i, 1, N + 1) {
      cin >> a;

      if (i % 2 == 0) {
        q.push_front(a);
      } else {
        q.push_back(a);
      }
    }
  } else {
    REPI(i, 1, N + 1) {
      cin >> a;
      if (i % 2 == 0) {
        q.push_back(a);
      } else {
        q.push_front(a);
      }
    }
  }

  REP(i, N) {
    cout << q.front() << " ";
    q.pop_front();
  }
  cout << endl;

  return 0;
}
