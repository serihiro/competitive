#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int main(){
    cin.tie(0);
    ios::sync_with_stdio(false);

    vector<long> r;
    long n;
    long c = 0;
    cin >> n;
    long sum = 0;
    long i;
    while(c < n && cin >> i){
        r.push_back(i);
        sum += i;
        ++c;
    }

    long max = *std::max_element(r.begin(), r.end());
    long min = *std::min_element(r.begin(), r.end());

    cout << min << " " << max << " " << sum << endl;

    return 0;
}
