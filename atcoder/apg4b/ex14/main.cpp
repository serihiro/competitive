#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, n)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int A, B, C;
  cin >> A >> B >> C;
  cout << max(max(A, B), C) - min(min(A, B), C) << endl;

  return 0;
}
