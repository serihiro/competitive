#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    cin.tie(0);
    ios::sync_with_stdio(false);

    long long N, A[5], B[5];
    cin >> N;
    cin >> A[0];
    cin >> A[1];
    cin >> A[2];
    cin >> A[3];
    cin >> A[4];

    long long max_count = -1;
    for (int i = 0; i < 5; ++i)
    {
        B[i] = N / A[i];
        if (N % A[i] != 0)
            ++B[i];
        if (max_count < B[i])
        {
            max_count = B[i];
        }
        else
        {
            B[i] = max_count;
        }
        B[i] += i;
    }

    long long result = B[0];
    for (int i = 1; i < 5; ++i)
    {
        result += (B[i] - B[i - 1]);
    }

    cout << result << endl;

    return 0;
}
