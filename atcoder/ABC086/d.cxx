// https://abc086.contest.atcoder.jp/tasks/arc089_b
// https://img.atcoder.jp/arc089/editorial.pdf
// give up
// 2018.01.27
// I retried, but time out occured in some test case :(
// https://abc086.contest.atcoder.jp/submissions/2020089

#include <iostream>
#include <vector>
#include <algorithm>
#include <array>
using namespace std;

#define REP(i, n) for (int i = 0; i < (n); ++i)

int main()
{
    cin.tie(0);
    ios::sync_with_stdio(false);

    long n, k;
    cin >> n >> k;
    int i = 0;
    long x, y;
    char c;
    constexpr int KMAX = 2001;
    static int map[2001][2001] = {};
    while (i < n && cin >> x >> y >> c)
    {
        if (c == 'B')
        {
            map[x % (k * 2)][y % (k * 2)] += 1;
        }
        else
        { // c == 'W'
            map[(x + k) % (k * 2)][y % (k * 2)] += 1;
        }
        ++i;
    }

    // REP(i, 10)
    // {
    //     REP(j, 10)
    //     {
    //         cout << map[i][j] << ",";
    //     }
    //     cout << endl;
    // }
    // cout << endl;

    /*
    (0,0)から順にそこが黒領域の左上だと仮定して
    他のmap上の黒になるべき領域が1になっている座標の
    数を数えていく

    (edge_x = 0, edge_y = 0)
    i)
    (edge_x...(edge_x + k - 1)) % (2 * k)
    (edge_y...(edge_y + k - 1)) % (2 * k)

    ii)
    (edge_x + k...(edge_x + k - 1)) % (2 * k)
    (edge_y + k...(edge_y + k - 1)) % (2 * k)

    oooxxx
    oooxxx
    oooxxx
    xxxooo
    xxxooo
    xxxooo

    (edge_x = 1, edge_y = 0)
    xoooxx
    xoooxx
    xoooxx
    oxxxoo
    oxxxoo
    oxxxoo
    */

    int max = 0;
    int count = 0;
    long edge_x = 0;
    long edge_y = 0;
    REP(i, k * 2 - 1)
    {
        REP(j, k * 2 - 1)
        {
            edge_x = i;
            edge_y = j;

            // if (edge_x % 100 == 0)
            // {
            //     cout << "edge_x: " << edge_x << endl;
            // }

            // if (edge_y % 100 == 0)
            // {
            //     cout << "edge_y: " << edge_y << endl;
            // }

            count = 0;

            for (int edge_x = i; edge_x < (i + k); edge_x++)
            {
                for (int edge_y = j; edge_y < (j + k); edge_y++)
                {
                    count += map[edge_x % (2 * k)][edge_y % (2 * k)] + map[(edge_x + k) % (2 * k)][(edge_y + k) % (2 * k)];
                }
            }
            if (count > max)
            {
                max = count;
            }
        }
    }

    cout << max << endl;

    return 0;
}
