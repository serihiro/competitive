#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int H, N;
  cin >> H >> N;
  vector<int> A(N);
  vector<int> B(N);
  int choice = -1;
  double cospa = 1000000.0;
  REP(i, N) {
    cin >> A[i] >> B[i];
    if (cospa > double(B[i]) / double(A[i])) {
      choice = i;
      cospa = double(B[i]) / double(A[i]);
    }
  }

  ll count = 0;
  count = H / A[choice];
  if (H % A[choice] != 0) {
    ++count;
  }
  cout << count * B[choice] << endl;

  return 0;
}
