#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N, K;
  cin >> N >> K;
  vector<ll> h(N);
  REP(i, N) { cin >> h.at(i); }
  sort(h.begin(), h.end());
  ll ans = 1e9;
  for (int i = 0; i < N - K + 1; ++i) {
    ans = min(h.at(i + K - 1) - h.at(i), ans);
  }

  cout << ans << endl;

  return 0;
}
