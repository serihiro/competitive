#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;

  vector<int> order(N, 0);
  REPI(i, 0, N) { order[i] = i + 1; }
  int seq = 1;
  string key = "";
  map<string, int> perm;
  do {
    key = "";
    REP(i, N) { key += to_string(order[i]); }
    perm.insert(make_pair(key, seq));
    ++seq;
  } while (next_permutation(order.begin(), order.end()));

  // for (auto e : perm) {
  //   cout << e.first << ":" << e.second << endl;
  // }

  int a, b;
  char tmp_c;
  string tmp_s = "";

  REP(i, N) {
    cin >> tmp_c;
    tmp_s += tmp_c;
  }
  a = perm.at(tmp_s);

  tmp_s = "";
  REP(i, N) {
    cin >> tmp_c;
    tmp_s += tmp_c;
  }
  b = perm.at(tmp_s);

  cout << abs(a - b) << endl;

  return 0;
}
