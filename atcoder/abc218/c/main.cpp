#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

pair<int, int> find_left_top(vector<vector<char>> a, int n) {
  REP(i, n) {
    REP(j, n) {
      if (a.at(i).at(j) == '#') {
        return make_pair(i, j);
      }
    }
  }

  return make_pair(-1, -1);
}

bool judge(vector<vector<char>> s, vector<vector<char>> t, int n) {
  pair<int, int> s_left_top = find_left_top(s, n);
  pair<int, int> t_left_top = find_left_top(t, n);
  int offset_i = t_left_top.first - s_left_top.first;
  int offset_j = t_left_top.second - s_left_top.second;
  // cout << offset_i << ":" << offset_j << endl;
  REP(i, n) {
    REP(j, n) {
      int ii = i + offset_i;
      int jj = j + offset_j;

      // Tにオフセットをつけてアクセスすることで，並行移動した状態でアクセスできる．賢い．
      if ((0 <= ii && ii < n) && (0 <= jj && jj < n)) {
        if (s.at(i).at(j) != t.at(ii).at(jj)) {
          return false;
        }
      } else {
        // Tの範囲外に#があったら一致する可能性がゼロになるので探索を打ち切る
        if (s.at(i).at(j) == '#') {
          return false;
        }
      }
    }
  }

  return true;
}

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;
  vector<vector<char>> S(N, vector<char>(N));
  int s_count = 0;
  REP(i, N) {
    REP(j, N) {
      cin >> S.at(i).at(j);
      if (S.at(i).at(j) == '#') {
        ++s_count;
      }
    }
  }

  vector<vector<char>> T(N, vector<char>(N));
  int t_count = 0;
  REP(i, N) {
    REP(j, N) {
      cin >> T.at(i).at(j);
      if (T.at(i).at(j) == '#') {
        ++t_count;
      }
    }
  }

  if (s_count != t_count) {
    cout << "No" << endl;
    return 0;
  }

  auto rotate = [](vector<vector<char>> a) {
    long n = a.size();
    vector<vector<char>> b(n, vector<char>(n));
    REP(i, n) {
      REP(j, n) { b.at(i).at(j) = a.at(n - 1 - j).at(i); }
    }
    return b;
  };

  REP(_, 4) {
    S = rotate(S);
    if (judge(S, T, N)) {
      cout << "Yes" << endl;
      return 0;
    }
  }

  cout << "No" << endl;

  return 0;
}
