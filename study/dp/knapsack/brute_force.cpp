// https://www.slideshare.net/iwiwi/ss-3578511 p.7
// O(2^N)

/*
case 1

6 8
2 1 3 2 1 5
3 2 6 1 3 85

91
*/
#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int N, U;
int W[101];
int V[101];

int search(int i, int u) {
  if (i == N) {
    return 0;
  } else if (u < W[i]) {
    return search(i + 1, u);
  } else {
    return max(search(i + 1, u), search(i + 1, u - W[i]) + V[i]);
  }
}

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  cin >> N >> U;
  REP(i, N) { cin >> W[i]; }
  REP(i, N) { cin >> V[i]; }

  int ans = search(0, U);
  cout << ans << endl;

  return 0;
}
