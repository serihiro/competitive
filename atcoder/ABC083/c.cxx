#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    cin.tie(0);
    ios::sync_with_stdio(false);

    long long x = 0;
    long long y = 0;
    cin >> x >> y;

    // An = x * 2 ^ (n - 1)より
    // y >= x * 2 ^ (n - 1
    // y / x >= 2 ^ (n - 1)
    // log(y) - log(x) >= n - 1
    // log(y) - log(x) + 1 >= n
    // を満たす最大の整数nを求めれば良さそう
    // だけどなんか計算誤差が出てるのがACできないサンプルが1個だけあるので愚直に解くことにする。。

    // long long n = floor(log2(y) - log2(x) + 1);
    // cout << n << endl;

    long long i = 0;
    while (x <= y)
    {
        x *= 2;
        ++i;
    }

    cout << i << endl;

    return 0;
}
