#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N, M;
  cin >> N >> M;
  map<int, int> limitation;
  vector<bool> visit(N + 1, false);

  int s, c;
  REP(i, M) {
    cin >> s >> c;
    if (visit.at(s)) {
      if (limitation.at(s) == c) {
        continue;
      } else {
        cout << -1 << endl;
        return 0;
      }
    }

    limitation.insert(make_pair(s, c));
    visit.at(s) = true;
  }

  REP(i, 10000) {
    stringstream ss;
    ss << i;
    string t_s = ss.str();
    if ((int)t_s.length() == N) {
      bool ok = true;
      for (auto const &it : limitation) {
        if (t_s.at(it.first - 1) != it.second + '0') {
          ok = false;
          break;
        }
      }

      if (ok) {
        cout << i << endl;
        return 0;
      }
    }
  }

  cout << -1 << endl;

  return 0;
}
