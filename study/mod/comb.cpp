#include <iostream>
#include <vector>
using namespace std;

const int MAX = 510000;
const int MOD = 1000000007;

typedef long long ll;
vector<ll> fac(MAX, 0ll);
vector<ll> finv(MAX, 0ll);
vector<ll> inv(MAX, 0ll);

void COMinit() {
  fac[0] = fac[1] = 1;
  finv[0] = finv[1] = 1;
  inv[1] = 1;
  for (int i = 2; i <= MAX; i++) {
    fac[i] = fac[i - 1] * i % MOD;
    inv[i] = MOD - inv[MOD % i] * (MOD / i) % MOD;
    finv[i] = finv[i - 1] * inv[i] % MOD;
  }
}

long long COM(int n, int k) {
  if (n < k)
    return 0;
  if (n < 0 || k < 0)
    return 0;
  // n! / (r!(n-r)!)
  return fac[n] * (finv[k] * finv[n - k] % MOD) % MOD;
}

int main() {
  COMinit();
  cout << COM(510000, 123123) << endl;
}