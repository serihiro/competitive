#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int H, W;
  cin >> H >> W;
  vector<vector<int>> A(H, vector<int>(W));
  vector<int> L(H, 0), C(W, 0);
  REP(i, H) {
    REP(j, W) {
      cin >> A.at(i).at(j);
      L.at(i) += A.at(i).at(j);
      C.at(j) += A.at(i).at(j);
    }
  }

  REP(i, H) {
    REP(j, W) {
      cout << L.at(i) + C.at(j) - A.at(i).at(j);
      if (j != W - 1) {
        cout << " ";
      }
    }
    cout << endl;
  }

  return 0;
}
