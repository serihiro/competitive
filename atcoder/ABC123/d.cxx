#include <iostream>
#include <algorithm>

using namespace std;

long long A[1000];
long long B[1000];
long long TMP1[1000000];
long long A_B[3000];
long long TMP2[3000000];
long long C[1000];
long long result[3000];

int main()
{
    cin.tie(0);
    ios::sync_with_stdio(false);

    int X, Y, Z, K;
    cin >> X >> Y >> Z >> K;
    for (int i = 0; i < X; ++i)
        cin >> A[i];
    for (int i = 0; i < Y; ++i)
        cin >> B[i];
    for (int i = 0; i < Z; ++i)
        cin >> C[i];

    for(int i=0; i<X; ++i){
        for(int j=0; j<Y; ++j){
            TMP1[i*Y + j] =  A[i] + B[j];
        }
    }
    sort(TMP1, TMP1 + 1000000, greater<long>());
    for(int i =0; i < K; ++i){
        A_B[i] = TMP1[i];
    }

    for(int i=0; i<K; ++i){
        for(int j=0; j<Z; ++j){
            TMP2[i*Z + j] = A_B[i] + C[j];
        }
    }
    sort(TMP2, TMP2 + 3000000, greater<long>());
    for(int i =0; i < K; ++i){
        cout << TMP2[i] << endl;
    }

    return 0;
}