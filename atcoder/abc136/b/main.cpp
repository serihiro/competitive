#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;
  ll ans = 0;
  REPI(i, 1, N + 1) {
    int keta = 0;
    int j = i;
    while (j > 0) {
      ++keta;
      j /= 10;
    }
    if (keta % 2 == 1) {
      ++ans;
    }
  }

  cout << ans << endl;

  return 0;
}
