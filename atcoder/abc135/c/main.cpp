#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;
  vector<ll> A(N + 1);
  REP(i, N + 1) { cin >> A.at(i); }
  vector<ll> B(N);
  REP(i, N) { cin >> B.at(i); }

  ll ans = 0;
  REP(i, N) {
    ll cnt1, cnt2;
    cnt1 = min(A.at(i), B.at(i));
    ans += cnt1;
    A.at(i) -= cnt1;
    B.at(i) -= cnt1;

    cnt2 = min(A.at(i + 1), B.at(i));
    ans += cnt2;
    A.at(i + 1) -= cnt2;
  }

  cout << ans << endl;

  return 0;
}
