#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  string S;
  cin >> S;

  int N = (int)S.length();
  REP(i, N / 2) {
    if (S[i] != S[N - 1 - i]) {
      cout << "No" << endl;
      return 0;
    }
  }

  REP(i, (N - 1) / 2) {
    if (S[i] != S[(N - 1) / 2 - 1 - i]) {
      cout << "No" << endl;
      return 0;
    }
  }

  REPI(i, 3, N) {
    if (S[i] != S[N - 1 - i]) {
      cout << "No" << endl;
      return 0;
    }
  }

  cout << "Yes" << endl;

  return 0;
}
