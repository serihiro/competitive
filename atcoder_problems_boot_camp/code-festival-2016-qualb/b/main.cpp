#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N, A, B;
  cin >> N >> A >> B;
  string S;
  cin >> S;

  int kokunai = 0;
  int kaigai = 0;

  REP(i, N) {
    bool passed = false;
    char c = S.at(i);
    // cout << kokunai << ":" << kaigai << endl;
    if (c == 'a') {
      if ((kokunai + kaigai) < (A + B)) {
        passed = true;
        ++kokunai;
      }
    } else if (c == 'b') {
      if (((kokunai + kaigai) < (A + B)) && (kaigai < B)) {
        passed = true;
        ++kaigai;
      }
    }

    if (passed)
      cout << "Yes" << endl;
    else
      cout << "No" << endl;
  }

  return 0;
}
