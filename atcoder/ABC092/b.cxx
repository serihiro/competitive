#include <iostream>
using namespace std;

int main()
{
    cin.tie(0);
    ios::sync_with_stdio(false);
    int N, D, X;
    int A[100];

    cin >> N;
    cin >> D >> X;
    for(int i = 0; i < N; ++i){
        cin >> A[i];
    }

    int a;
    int sum = 0;
    for(int i = 0; i < N; ++i){
        a = A[i];
        for(int j = 0; j < D; ++j){
            if( j * a + 1 > D ){
                break;
            }
            ++sum;
        }
    }
        
    cout << sum + X << endl;

    return 0;
}
