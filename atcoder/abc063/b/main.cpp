#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  string S;
  cin >> S;
  vector<int> count(26, 0);
  int l = S.length();
  REP(i, l) {
    int index = S.at(i) - 'a';
    if (count.at(index) > 0) {
      cout << "no" << endl;
      return 0;
    }
    ++count.at(index);
  }

  cout << "yes" << endl;

  return 0;
}
