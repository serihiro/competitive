#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int n;
  cin >> n;
  int t_i = 0, h_i = 0;
  string t_s, h_s;
  REP(i, n) {
    cin >> t_s >> h_s;
    int result = t_s.compare(h_s);
    if (result == 0) {
      ++t_i;
      ++h_i;
    } else if (result > 0) {
      t_i += 3;
    } else {
      h_i += 3;
    }
  }

  cout << t_i << " " << h_i << endl;

  return 0;
}
