#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  int P[20];

  cin >> N;
  REP(i, N) { cin >> P[i]; }

  int ans = 0;

  REP(i, N - 2) {
    if ((P[i] < P[i + 1] && P[i + 1] < P[i + 2]) ||
        (P[i] > P[i + 1] && P[i + 1] > P[i + 2])) {
      ans++;
    }
  }

  cout << ans << endl;

  return 0;
}
