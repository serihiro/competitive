#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  double V[50];

  cin >> N;
  REP(i, N) { cin >> V[i]; }
  sort(V, V + N);

  for (int i = 0; i < N - 1; ++i) {
    V[i + 1] = (V[i] + V[i + 1]) / 2;
  }

  cout << setprecision(10) << V[N - 1] << endl;

  return 0;
}
