#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;
  vector<int> X(N + 1);
  REP(i, N) { cin >> X.at(i + 1); }
  int max_x = *max_element(X.begin(), X.end());
  int min_x = *min_element(X.begin(), X.end());

  ll ans = 1e9;
  REPI(i, min_x, max_x + 1) {
    ll tmp_ans = 0;
    REPI(j, 1, N + 1) { tmp_ans += (X.at(j) - i) * (X.at(j) - i); }

    ans = min(ans, tmp_ans);
  }

  cout << ans << endl;

  return 0;
}
