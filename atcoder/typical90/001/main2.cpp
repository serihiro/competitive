#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

bool solve(const int mid, vector<ll> &a, const ll k, const ll l, const ll n) {
  int count = 0;
  int pre = 0;
  REP(i, n) {
    int current = a.at(i) - pre;
    int remaining = l - a.at(i);
    if (current >= mid && remaining >= mid) {
      ++count;
      pre = a.at(i);
    }
  }

  if (count >= k) {
    return true;
  }
  return false;
}

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  ll K, L, N;
  cin >> N >> L >> K;
  vector<ll> A(N);
  REP(i, N) { cin >> A.at(i); }

  int ok = 0, ng = L;
  while (abs(ok - ng) > 1) {
    // cout << ok << ":" << ng << endl;

    int mid = (ok + ng) / 2;
    if (solve(mid, A, K, L, N)) {
      ok = mid;
    } else {
      ng = mid;
    }
  }

  cout << ok << endl;

  return 0;
}
