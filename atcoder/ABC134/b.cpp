#include <algorithm>
#include <bits/stdc++.h>
#include <cassert>
#include <functional>

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N, D;
  cin >> N >> D;

  int ans = 1;
  while (2 * D * ans + (ans - 1) < N - 1) {
    ans++;
  }

  cout << ans << endl;
  //   int ans = static_cast<int>(ceil((N - 1) / (2.0 * D + 1)));

  return 0;
}
