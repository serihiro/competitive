#include <iostream>
#include <algorithm>

using namespace std;

long long N;
char S[200000];

int main()
{
    cin.tie(0);
    ios::sync_with_stdio(false);

    cin >> N;
    cin >> S;

    bool found_black = false;
    bool found_white = false;
    long long result_1 = 0;
    long long result_2 = 0;

    for (long long i = 0; i < N; ++i)
    {
        if (S[i] == '.')
        {
            if (found_black)
            {
                ++result_1;
            }
        }
        else
        {
            found_black = true;
        }
    }

    for (long long i = N - 1; i >= 0; --i)
    {
        if (S[i] == '#')
        {
            if (found_white)
            {
                ++result_2;
            }
        }
        else
        {
            found_white = true;
        }
    }

    cout << result_1 << ":" << result_2 << endl;

    if (result_1 < result_2)
    {
        cout << result_1 << endl;
    }
    else
    {
        cout << result_2 << endl;
    }

    return 0;
}
