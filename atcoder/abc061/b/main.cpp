#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N, M;
  cin >> N >> M;
  vector<vector<int>> G(N);
  int a, b;
  REP(i, M) {
    cin >> a >> b;
    --a;
    --b;
    G.at(a).push_back(b);
    G.at(b).push_back(a);
  }

  REP(i, N) { cout << G.at(i).size() << endl; }

  return 0;
}
