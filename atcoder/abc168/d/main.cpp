#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N, M;
  cin >> N >> M;
  vector<vector<int>> G(N);
  int a, b;
  REP(i, M) {
    cin >> a >> b;
    --a;
    --b;
    G.at(a).push_back(b);
    G.at(b).push_back(a);
  }

  vector<bool> visit(N, false);
  queue<int> q;
  unordered_map<int, int> route;

  q.push(0);
  while (!q.empty()) {
    int v = q.front();
    q.pop();

    for (int nv : G.at(v)) {
      if (!visit.at(nv)) {
        q.push(nv);
        visit.at(nv) = true;
        route.insert(make_pair(nv, v));
      }
    }
  }

  if (!all_of(visit.begin(), visit.end(), [](bool f) { return f; })) {
    cout << "No" << endl;
    return 0;
  }

  cout << "Yes" << endl;
  REPI(i, 1, N) {
    // cout << i << ":" << route.at(i).size() << endl;
    cout << route.at(i) + 1 << endl;
  }

  return 0;
}
