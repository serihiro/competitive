#include <iostream>
#include <vector>
#include <cstdlib>
using namespace std;
long as[100000];

int main()
{
    cin.tie(0);
    ios::sync_with_stdio(false);

    long n;
    cin >> n;

    long a;
    long n_kind = 0;

    for (long i = 0; i < n; ++i)
    {
        cin >> a;
        a--;
        if (as[a] == 0)
        {
            ++n_kind;
        }
        ++as[a];
    }

    if (n_kind == n)
    {
        cout << n_kind << endl;
        return 0;
    }

    if (n_kind % 2 == 0)
    {
        cout << n_kind - 1 << endl;
    }
    else
    {
        cout << n_kind << endl;
    }

    return 0;
}

// int main()
// {
//     cin.tie(0);
//     ios::sync_with_stdio(false);

//     long n;
//     cin >> n;

//     long a;
//     vector<long> as;
//     long cnt = 0;
//     while (cnt < n && cin >> a){
//         as.push_back(a);
//         ++cnt;
//     }

//     std::sort(as.begin(), as.end());
//     as.erase(std::unique(as.begin(), as.end()), as.end());

//     long result = as.size() % 2 == 0 ? as.size() - 1 : as.size();

//     cout << result << endl;

//     return 0;
// }
