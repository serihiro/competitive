#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int i = 1;
  int x;
  while (cin >> x) {
    if (x == 0) {
      return 0;
    }

    cout << "Case " << i << ": " << x << endl;
    ++i;
  }

  return 0;
}
