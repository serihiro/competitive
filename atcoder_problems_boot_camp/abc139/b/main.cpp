#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int A, B;
  cin >> A >> B;

  if (B == 1) {
    cout << 0 << endl;
    return 0;
  }

  int ans = 1;
  int total = A;
  while (total < B) {
    ++ans;
    total += (A - 1);
  }

  cout << ans;

  return 0;
}
