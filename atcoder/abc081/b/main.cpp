#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;
  vector<ll> a(N);
  REP(i, N) { cin >> a.at(i); }

  ll ans = 1 << 29;
  REP(i, N) {
    ll tmp_a = a.at(i);
    ll count = 0;
    while (true) {
      if (tmp_a % 2 != 0) {
        break;
      }
      ++count;
      tmp_a /= 2;
    }
    ans = min(ans, count);
  }

  cout << ans << endl;

  return 0;
}
