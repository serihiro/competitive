#include <iostream>
#include <cstdlib>
using namespace std;
int main(){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int S;
    cin >> S;

    if(S == 0) {
        cout << "0:0:0\n";
        return 0;
    }

    if(S < 60) {
        cout << "0:0:" << S << "\n";
        return 0;
    }

    int h = S / 3600;
    int minutes = S - h * 3600;
    int m = minutes / 60;
    int s = ( minutes - ( m * 60 ) ) % 60;

    cout << h << ":" << m << ":" << s << "\n";
    return 0;
}
