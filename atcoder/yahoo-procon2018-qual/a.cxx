#include <iostream>
#include <string>
#include <algorithm>
using namespace std;

#define REP(i, n) for (int i = 0; i < (n); ++i)

int main()
{
    cin.tie(0);
    ios::sync_with_stdio(false);

    string str = "";
    cin >> str;

    string yahoo_prefix = "yah";

    if (equal(begin(yahoo_prefix), end(yahoo_prefix), begin(str)) && str[3] == str[4])
    {
        cout << "YES";
    }
    else
    {
        cout << "NO";
    }

    return 0;
}
