#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int comb_2(int a) {
  if (a == 1) {
    return 1;
  }

  int ans = 0;
  ans = a * (a - 1) / 2;
  return ans;
}

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N, M;
  cin >> N >> M;

  int ans = 0;
  if (N >= 2) {
    ans += comb_2(N);
  }

  if (M >= 2) {
    ans += comb_2(M);
  }

  cout << ans << endl;

  return 0;
}
