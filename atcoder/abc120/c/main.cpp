#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  string S;
  cin >> S;
  stack<char> s;
  s.push(S.at(0));
  int l = S.length();
  int ans = 0;
  for (int i = 1; i < l; ++i) {
    if (s.size() > 0 && s.top() != S.at(i)) {
      ans += 2;
      s.pop();
    } else {
      s.push(S.at(i));
    }
  }

  cout << ans << endl;
  return 0;
}
