#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;

  ll L0 = 2;
  ll L1 = 1;
  if (N == 1) {
    cout << L1 << endl;
    return 0;
  }

  ll L;
  REPI(i, 2, N + 1) {
    L = L0 + L1;
    L0 = L1;
    L1 = L;
  }

  cout << L << endl;

  return 0;
}
