#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  string S;
  cin >> S;
  int l = S.length();
  int ans = 1 << 29;
  for (int i = 0; i < l - 2; ++i) {
    int a =
        (S.at(i) - '0') * 100 + (S.at(i + 1) - '0') * 10 + (S.at(i + 2) - '0');

    ans = min(ans, abs(753 - a));
  }

  cout << ans << endl;

  return 0;
}
