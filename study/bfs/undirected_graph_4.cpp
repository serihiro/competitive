#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;
using Graph = vector<vector<int>>; // G[E][V]

int main() {
  int N; // N is the size of the vertexes.
  int M; // M is the size of the edges.
  cin >> N >> M;

  Graph G(N);
  REP(i, M) {
    int a, b;
    cin >> a >> b;
    G.at(a).push_back(b);
    G.at(b).push_back(a);
  }

  vector<int> dist(N, -1);
  queue<int> Q;

  // set the start.
  Q.push(0);
  dist.at(0) = 0;

  while (!Q.empty()) {
    int v = Q.front();
    Q.pop();

    for (int nv : G.at(v)) {
      if (dist.at(nv) != -1) {
        continue;
      }

      dist.at(nv) = dist.at(v) + 1;
      Q.push(nv);
    }
  }

  REP(i, N) { cout << i << ":" << dist[i] << endl; }

  return 0;
}
