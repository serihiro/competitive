#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

vector<int> a, b, c, d;
int N, M, Q;
int ans;

void dfs(vector<int> A, int size) {
  // for (int const &i : A) {
  //   cout << i << ",";
  // }
  // cout << endl;

  if (size == N) {
    int now = 0;
    REP(i, Q) {
      if (A.at(b.at(i)) - A.at(a.at(i)) == c.at(i)) {
        now += d.at(i);
      }
    }
    ans = max(ans, now);
    return;
  }

  A.push_back(A.back());
  while (A.back() <= M) {
    dfs(A, size + 1);
    ++A.back();
  }
}

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  cin >> N >> M >> Q;
  a = b = c = d = vector<int>(Q);
  REP(i, Q) {
    cin >> a.at(i) >> b.at(i) >> c.at(i) >> d.at(i);
    --a.at(i);
    --b.at(i);
  }
  ans = 0;
  dfs(vector<int>(1, 1), 1);

  cout << ans << endl;

  return 0;
}
