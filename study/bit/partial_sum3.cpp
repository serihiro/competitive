#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;
  vector<int> a(N);
  REP(i, N) { cin >> a.at(i); }
  int K;
  cin >> K;

  bool ans = false;
  for (int i = 0; i < (1 << N); ++i) {
    ll sum = 0;
    REP(j, N) {
      if (i & (1 << j)) {
        sum += a.at(j);
      }
    }
    if (sum == K) {
      ans = true;
      break;
    }
  }

  if (ans) {
    cout << "Yes" << endl;
  } else {
    cout << "No" << endl;
  }

  return 0;
}
