#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int n;
  cin >> n;

  int i = 1;
  while (i <= n) {
    int x = i;
    if (x % 3 == 0) {
      cout << " " << i;
      ++i;
      continue;
    } else if (x % 10 == 3) {
      cout << " " << i;
      ++i;
      continue;
    }

    x /= 10;
    while (x > 0) {
      if (x % 10 == 3) {
        cout << " " << i;
        break;
      }
      x /= 10;
    }

    ++i;
  }

  cout << endl;

  return 0;
}
