#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  int M;
  cin >> N >> M;

  vector<vector<int>> G(N);

  REP(i, M) {
    int a, b;
    cin >> a >> b;
    G.at(a).push_back(b);
    G.at(b).push_back(a);
  }

  vector<int> dist(N, -1);
  queue<int> q;

  dist.at(0) = 0;
  q.push(0);

  while (!q.empty()) {
    int v = q.front();
    q.pop();

    for (int nv : G[v]) {
      if (dist[nv] != -1)
        continue;

      dist[nv] = dist[v] + 1;
      q.push(nv);
    }
  }

  REP(i, N) { cout << i << ":" << dist[i] << endl; }

  return 0;
}