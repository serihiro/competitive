#include <iostream>
#include <vector>
#include <cstdlib>
using namespace std;

int main(){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int n;
    cin >> n;

    int a;
    vector<long> as;
    long c = 0;
    long odd_c = 0;
    long even_c = 0;
    while (c < n && cin >> a){
        as.push_back(a);
        if (a % 2 == 0){
            ++even_c;
        }else{
            ++odd_c;
        }
        ++c;
    }

    if(odd_c % 2 == 0){
        cout << "YES" << endl;
    }else{
        cout << "NO" << endl;
    }

    return 0;
}
