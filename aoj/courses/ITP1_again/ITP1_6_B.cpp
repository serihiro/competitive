#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int char2int(char c) {
  if (c == 'S') {
    return 0;
  } else if (c == 'H') {
    return 1;
  } else if (c == 'C') {
    return 2;
  } else {
    return 3;
  }
}

char int2char(int i) {
  if (i == 0) {
    return 'S';
  } else if (i == 1) {
    return 'H';
  } else if (i == 2) {
    return 'C';
  } else {
    return 'D';
  }
}

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int n;
  cin >> n;
  vector<vector<bool>> C(4, vector<bool>(13, false));

  REP(i, n) {
    char c;
    int num;
    cin >> c >> num;
    --num;
    C.at(char2int(c)).at(num) = true;
  }

  REP(i, 4) {
    REP(j, 13) {
      if (!C.at(i).at(j)) {
        cout << int2char(i) << " " << j + 1 << endl;
      }
    }
  }

  return 0;
}
