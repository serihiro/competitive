#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;
  while (N > 10) {
    N %= 10;
  }

  if (N == 3) {
    cout << "bon" << endl;
  } else if (N == 0 || N == 1 || N == 6 || N == 8) {
    cout << "pon" << endl;
  } else {
    cout << "hon" << endl;
  }

  return 0;
}
