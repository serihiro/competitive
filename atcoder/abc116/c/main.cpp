#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;
  vector<int> h(N);
  REP(i, N) { cin >> h.at(i); }

  int ans = 0;
  int l;
  while (true) {
    l = -1;
    REP(i, N) {
      // cout << i << ":" << h.at(i) << endl;
      if (h.at(i) > 0) {
        if (l == -1) {
          l = i;
        }
        --h.at(i);
      } else {
        if (l != -1) {
          break;
        }
      }
    }

    if (l == -1) {
      break;
    }
    ++ans;
  }

  cout << ans << endl;
  return 0;
}
