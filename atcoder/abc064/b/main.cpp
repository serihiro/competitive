#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;
  vector<int> A(N);
  REP(i, N) { cin >> A.at(i); }
  sort(A.begin(), A.end());

  int ans = 1 << 29;
  REP(i, N) { ans = min(ans, A.at(N - 1) - A.at(i) + (A.at(i) - A.at(0))); }

  cout << ans << endl;

  return 0;
}
