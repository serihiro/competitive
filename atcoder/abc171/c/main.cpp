#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  ll N;
  cin >> N;

  string ans = "";

  if (N <= 26) {
    cout << char('a' + N - 1) << endl;
    return 0;
  }

  while (N > 0) {
    --N;
    ans += (char)('a' + (N % 26));
    N /= 26;
  }

  reverse(ALL(ans));
  cout << ans << endl;

  return 0;
}
