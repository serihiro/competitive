#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  vector<vector<int>> A(3);
  int a;
  REP(i, 3) {
    REP(j, 3) {
      cin >> a;
      A.at(i).push_back(a);
    }
  }
  int N;
  cin >> N;
  int b;
  REP(i, N) {
    cin >> b;
    REP(i, 3) {
      REP(j, 3) {
        if (A.at(i).at(j) == b) {
          A.at(i).at(j) = -1;
          break;
        }
      }
    }
    REP(i, 3) {
      if (A.at(i).at(0) == -1 && A.at(i).at(1) == -1 && A.at(i).at(2) == -1) {
        cout << "Yes" << endl;
        return 0;
      }

      if (A.at(0).at(i) == -1 && A.at(1).at(i) == -1 && A.at(2).at(i) == -1) {
        cout << "Yes" << endl;
        return 0;
      }
    }

    if ((A.at(0).at(0) == -1 && A.at(1).at(1) == -1 && A.at(2).at(2) == -1) ||
        (A.at(0).at(2) == -1 && A.at(1).at(1) == -1 && A.at(2).at(0) == -1)) {
      cout << "Yes" << endl;
      return 0;
    }
  }

  cout << "No" << endl;
  return 0;
}
