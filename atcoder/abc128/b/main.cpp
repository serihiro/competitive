#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;
  map<string, priority_queue<int>> S;
  map<int, int> P;
  string s;
  int p;
  REP(i, N) {
    cin >> s >> p;
    if (S.find(s) == S.end()) {
      priority_queue<int> _p;
      _p.push(p);
      S.insert(make_pair(s, _p));
    } else {
      S.at(s).push(p);
    }
    P.insert(make_pair(p, i));
  }

  for (auto it : S) {
    while (!it.second.empty()) {
      cout << P.at(it.second.top()) + 1 << endl;
      it.second.pop();
    }
  }

  return 0;
}
