#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  int d[100];

  cin >> N;
  REP(i, N) { cin >> d[i]; }

  int result = 0;
  for (int i = 0; i < N; ++i) {
    for (int j = i + 1; j < N; ++j) {
      result += (d[i] * d[j]);
    }
  }

  cout << result << endl;

  return 0;
}
