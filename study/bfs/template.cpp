void dfs(vector<vector<int>> &G, vector<int> &dist, int v) {
  dist.at(v) = 0;
  queue<int> Q;
  Q.push(v);
  while (!Q.empty()) {
    int v = Q.front();
    Q.pop();

    for (int nv : G.at(v)) {
      if (dist.at(nv) == -1) {
        dist.at(nv) = dist.at(v) + 1;
        Q.push(nv);
      }
    }
  }
}
