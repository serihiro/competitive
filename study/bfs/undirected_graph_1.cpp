/*
9 13
0 1
0 4
0 2
1 4
1 3
1 8
2 5
3 8
4 8
5 8
5 6
3 7
6 7
*/

#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;
using Graph = vector<vector<int>>; // G[E][V]
// V = vertex
// E = edge

int main() {
  int N; // N is the size of the vertexes.
  int M; // M is the size of the edges.
  cin >> N >> M;
  Graph G(N);
  REP(i, M) {
    int a, b;
    cin >> a >> b;
    G.at(a).push_back(b);
    G.at(b).push_back(a);
  }

  vector<int> dist(N, -1);
  queue<int> q;
  dist.at(0) = 0;
  q.push(0);

  while (!q.empty()) {
    int v = q.front();
    q.pop();

    for (int nv : G[v]) {
      if (dist[nv] != -1) {
        continue;
      }

      dist[nv] = dist[v] + 1;
      q.push(nv);
    }
  }

  REP(i, N) { cout << i << ":" << dist[i] << endl; }
}