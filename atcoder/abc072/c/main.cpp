#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;
  int a;
  map<int, int> count;
  REP(i, N) {
    cin >> a;
    if (count.find(a - 1) == count.end()) {
      count.insert(make_pair((a - 1), 1));
    } else {
      ++count.at(a - 1);
    }

    if (count.find(a) == count.end()) {
      count.insert(make_pair((a), 1));
    } else {
      ++count.at(a);
    }

    if (count.find(a + 1) == count.end()) {
      count.insert(make_pair((a + 1), 1));
    } else {
      ++count.at(a + 1);
    }
  }

  int max_count = 0;
  for (auto const &it : count) {
    max_count = max(max_count, it.second);
  }

  cout << max_count << endl;

  return 0;
}
