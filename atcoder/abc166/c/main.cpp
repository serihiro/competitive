#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N, M;
  cin >> N >> M;
  vector<ll> H(N);
  REP(i, N) { cin >> H.at(i); }

  vector<vector<int>> path(N);
  int a, b;
  REP(i, M) {
    cin >> a >> b;
    --a;
    --b;
    path.at(a).push_back(b);
    path.at(b).push_back(a);
  }

  int ans = 0;
  vector<bool> visit(N, false);
  REP(i, N) {
    if (path.at(i).size() == 0 || visit.at(i)) {
      continue;
    }

    visit.at(i) = true;
    ll h = H.at(i);
    bool highest = true;

    for (int const &d : path.at(i)) {
      if (H.at(d) >= h) {
        highest = false;
        break;
      }
    }
    if (highest) {
      for (int const &d : path.at(i)) {
        visit.at(d) = true;
      }
      ++ans;
    }
  }

  REP(i, N) {
    if (!visit.at(i)) {
      ++ans;
    }
  }

  cout << ans << endl;

  return 0;
}
