#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N, A, X, Y;
  int ans = 0;
  cin >> N >> A >> X >> Y;
  if (N > A) {
    ans = A * X + (N - A) * Y;
  } else {
    ans = N * X;
  }

  cout << ans << endl;

  return 0;
}
