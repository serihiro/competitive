#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  string S;
  cin >> S;
  ll K;
  cin >> K;

  ll l = S.length();
  if (l == 1 || K == 1) {
    cout << S.at(0) << endl;
    return 0;
  }

  for (ll i = 0; i < K; ++i) {
    if (S.at(i) != '1') {
      cout << S.at(i) << endl;
      return 0;
    }
  }
  cout << 1 << endl;

  return 0;
}
