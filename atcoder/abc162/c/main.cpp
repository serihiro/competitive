// #include <bits/stdc++.h>

// #define REP(i, x) REPI(i, 0, x)
// #define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
// #define ALL(x) (x).begin(), (x).end()

// typedef long long ll;
// using namespace std;

// int main() {
//   cin.tie(0);
//   ios::sync_with_stdio(false);

//   int K;
//   cin >> K;
//   map<set<int>, int> memo;

//   ll ans = 0;

//   REPI(a, 1, K + 1) {
//     REPI(b, 1, K + 1) {
//       REPI(c, 1, K + 1) {
//         if (a == b && b == c) {
//           ans += a;
//           continue;
//         }

//         set<int> s = {a, b, c};
//         if (memo.find(s) != memo.end()) {
//           ans += memo.at(s);
//         } else {
//           int g = __gcd(__gcd(a, b), c);
//           memo.insert(make_pair(s, g));
//           ans += g;
//         }
//       }
//     }
//   }

//   cout << ans << endl;

//   return 0;
// }

#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int K;
  cin >> K;
  if (K == 1) {
    cout << 1 << endl;
    return 0;
  }

  map<pair<int, int>, int> memo;
  ll ans = 0;

  REPI(i, 1, K + 1) {
    REPI(j, 1, K + 1) { memo.insert(make_pair(make_pair(i, j), __gcd(i, j))); }
  }

  for (auto it : memo) {
    REPI(k, 1, K + 1) { ans += memo.at(make_pair(it.second, k)); }
  }

  cout << ans << endl;

  return 0;
}