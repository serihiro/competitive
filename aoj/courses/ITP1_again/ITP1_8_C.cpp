#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

char down_case(char c) {
  if ('a' <= c && c <= 'z') {
    return c;
  } else {
    return char('a' + (c - 'A'));
  }
}

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  vector<int> c(26, 0);

  while (!cin.eof()) {
    string s;
    getline(cin, s);

    REP(i, (int)s.length()) {
      if (('a' <= s.at(i) && s.at(i) <= 'z') ||
          ('A' <= s.at(i) && s.at(i) <= 'Z')) {
        ++c.at((int)down_case(s.at(i)) - 'a');
      }
    }
  }

  REP(i, 26) { cout << char('a' + i) << " : " << c.at(i) << endl; }

  return 0;
}
