#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int H, W;
  cin >> H >> W;
  vector<vector<char>> map;
  string S;
  REP(i, H) {
    map.push_back({});
    cin >> S;
    REP(j, W) {
      for (char const &c : S) {
        map.at(i).push_back(c);
      }
    }
  }

  REP(i, H) {
    REP(j, W) {
      int count = 0;

      if (map.at(i).at(j) == '.') {
        for (int y = -1; y <= 1; ++y) {
          for (int x = -1; x <= 1; ++x) {
            int nx = j + x;
            int ny = i + y;

            if ((-1 < nx && nx < W) && (-1 < ny && ny < H)) {
              if (map.at(ny).at(nx) == '#') {
                // cout << i << ":" << j << "/" << ny << ":" << nx << endl;
                ++count;
              }
            }
          }
        }

        map.at(i).at(j) = char('0' + count);
      }
    }
  }

  REP(i, H) {
    REP(j, W) { cout << map.at(i).at(j); }
    cout << endl;
  }

  return 0;
}