// https://arc090.contest.atcoder.jp/tasks/arc090_a
// https://arc090.contest.atcoder.jp/submissions/2033214
// give up!!!!!

#include <algorithm>
#include <array>
#include <iostream>
#include <vector>
using namespace std;

#define REP(i, n) for (int i = 0; i < (n); ++i)

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int n;
  int tmp;
  cin >> n;

  static int map[101][3] = {};

  for (int i = 1; i < 3; i++) {
    for (int j = 1; j < n + 1; j++) {
      cin >> tmp;
      map[i][j] = tmp;
    }
  }

  int x = 1;
  int y = 1;
  int prev_x = 1;
  int prev_y = 1;
  int sum = map[1][1];
  int max = 0;

  while (true) {
    // cout << "x: " << x << ", y: " << y << ", sum: " << sum << endl;

    if (y == 1) {
      if (x < n) {
        if (map[y][x + 1] >= map[y + 1][x]) {
          prev_x = x;
          ++x;
        } else {
          prev_y = y;
          ++y;
        }
      } else {
        prev_y = y;
        ++y;
      }
    } else {
      prev_x = x;
      ++x;
    }

    sum += map[y][x];

    if (x == n && y == 2) {
      break;
    }
  }

  cout << sum << endl;
  return 0;
}
