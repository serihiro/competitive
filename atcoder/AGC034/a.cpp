#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N, A, B, C, D;
  string S;
  cin >> N >> A >> B >> C >> D;
  cin >> S;

  for (int i = A + 1; i < C; ++i) {
    if ((S[i - 1] == '#') && (S[i] == '#')) {
      cout << "No" << endl;
      return 0;
    }
  }

  for (int i = B + 1; i < D; ++i) {
    if ((S[i - 1] == '#') && (S[i] == '#')) {
      cout << "No" << endl;
      return 0;
    }
  }

  bool swappable = false;
  if (C > D) {
    for (int i = B - 1; i < D; ++i) {
      if (S[i - 1] == '.' && S[i] == '.' && S[i + 1] == '.') {
        swappable = true;
        break;
      }
    }

    if (!swappable) {
      cout << "No" << endl;
      return 0;
    }
  }

  cout << "Yes" << endl;

  return 0;
}
