#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N, M;
  int wa = 0;
  int ac = 0;
  map<int, bool> aced;

  cin >> N >> M;
  vector<int> waed(N + 1, 0);
  REPI(i, 1, N + 1) { aced.insert(make_pair(i, false)); }

  int p;
  string s;
  REP(i, M) {
    cin >> p >> s;
    if (!aced.at(p)) {
      if (s == "WA") {
        ++waed.at(p);
      }

      if (s == "AC") {
        ++ac;
        wa += waed.at(p);
        aced.at(p) = true;
      }
    }
  }

  cout << ac << " " << wa << endl;

  return 0;
}