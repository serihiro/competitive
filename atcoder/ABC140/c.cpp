
#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  int B[100];
  cin >> N;
  REP(i, N - 1) { cin >> B[i]; }

  int ans = B[0];
  for (int i = 1; i < N - 1; ++i) {
    ans += min(B[i], B[i - 1]);
  }
  ans += B[N - 2];

  cout << ans << endl;

  return 0;
}
