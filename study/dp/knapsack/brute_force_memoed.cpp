// https://www.slideshare.net/iwiwi/ss-3578511 p.16
// O(NU)

/*
case 1

6 8
2 1 3 2 1 5
3 2 6 1 3 85

91
*/
#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int N, U;
int W[101];
int V[101];
bool done[101][100001];
int memo[101][100001];

int search(int i, int u) {
  int res, res1, res2;

  if (i == N) {
    res = 0;
  } else if (u < W[i]) {
    res = done[i][u] ? memo[i][u] : search(i + 1, u);
  } else {
    res1 = done[i][u] ? memo[i][u] : search(i + 1, u);
    res2 = done[i][u] ? memo[i][u] : search(i + 1, u - W[i]) + V[i];
    res = max(res1, res2);
  }

  if (!done[i][u]) {
    done[i][u] = true;
    memo[i][u] = res;
  }

  return res;
}

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  REP(i, 101) {
    REP(j, 100001) {
      done[i][j] = false;
      memo[i][j] = 0;
    }
  }

  cin >> N >> U;
  REP(i, N) { cin >> W[i]; }
  REP(i, N) { cin >> V[i]; }

  int ans = search(0, U);
  cout << ans << endl;

  return 0;
}
