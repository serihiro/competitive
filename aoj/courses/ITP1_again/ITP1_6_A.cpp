#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int n;
  cin >> n;
  vector<int> a(n);
  REP(i, n) { cin >> a.at(i); }
  REP(i, n) {
    cout << a.at(n - 1 - i);
    if (i != n - 1) {
      cout << " ";
    }
  }
  cout << endl;

  return 0;
}
