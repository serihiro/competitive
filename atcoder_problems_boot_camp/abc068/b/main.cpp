#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;
  int ans = 1;
  if (N == 1) {
    cout << ans << endl;
    return 0;
  }

  int max_count = 0;
  REPI(i, 2, N + 1) {
    int j = i;
    int count = 0;
    while (true) {
      if (j % 2 == 0) {
        j /= 2;
        ++count;
      } else {
        break;
      }
    }

    if (count > max_count) {
      ans = i;
      max_count = count;
    }
  }

  cout << ans << endl;

  return 0;
}
