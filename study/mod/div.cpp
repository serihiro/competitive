#include <iostream>
using namespace std;

typedef long long ll;
int const MOD = 1000000007;

ll modinv(ll a, ll m) {
  ll b = m;
  ll u = 1;
  ll v = 0;
  ll t = a / b;

  while (b) {
    t = a / b;
    a -= t * b;
    swap(a, b);
    u -= t * v;
    swap(u, v);
  }
  u %= m;
  if (u < 0)
    u += m;

  return u;
}

int main() {

  ll a = 12345678900000;
  ll b = 100000;
  a %= MOD;
  cout << a * modinv(b, MOD) % MOD << endl;

  return 0;
}
