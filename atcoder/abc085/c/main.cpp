#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  ll N, Y;
  cin >> N >> Y;

  for (ll i = 0; i <= N; ++i) {
    for (ll j = 0; j <= N - i; ++j) {
      ll k = N - i - j;
      if (i * 10000 + j * 5000 + k * 1000 == Y) {
        cout << i << " " << j << " " << k << endl;
        return 0;
      }
    }
  }

  cout << "-1 -1 -1" << endl;

  return 0;
}
