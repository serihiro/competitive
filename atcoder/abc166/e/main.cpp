#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

ll c2(ll n) {
  if (n <= 2) {
    return n;
  }
  return n * (n - 1) / 2;
}

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;
  map<ll, set<ll>> L;
  map<ll, set<ll>> R;
  ll a;
  for (ll i = 1; i <= N; ++i) {
    cin >> a;
    ll l = i + a;
    ll r = i - a;

    if (L.find(l) == L.end()) {
      set<ll> s;
      s.insert(i);
      L.insert(make_pair(l, s));
    } else {
      L.at(l).insert(i);
    }

    if (r > 0) {
      if (R.find(r) == R.end()) {
        set<ll> s;
        s.insert(i);
        R.insert(make_pair(r, s));
      } else {
        R.at(r).insert(i);
      }
    }
  }

  ll ans = 0;
  for (auto const &it : L) {
    if (R.find(it.first) != R.end()) {
      ans += it.second.size() * R.at(it.first).size();
    }
  }

  cout << ans << endl;

  return 0;
}
