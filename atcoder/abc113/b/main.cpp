#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);
  int N, T, A;
  cin >> N >> T >> A;
  int h;
  vector<double> diff;
  REP(i, N) {
    cin >> h;
    diff.push_back(abs(A - (T - (h * 0.006))));
  }

  auto min_diff = min_element(diff.begin(), diff.end());
  int ans = std::distance(diff.begin(), min_diff);
  cout << ans + 1 << endl;

  return 0;
}
