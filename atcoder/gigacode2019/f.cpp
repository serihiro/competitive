#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int H, W, N, tmp_r, tmp_c;
  int r[201901];
  int c[201901];

  REP(i, 201901) {
    r[i] = 0;
    c[i] = 0;
  }

  cin >> H >> W >> N;
  REP(i, N) {
    cin >> tmp_r;
    cin >> tmp_c;
    r[tmp_r] = 1;
    c[tmp_c] = 1;
  }

  if (N == 0) {
    cout << 1 << endl;
    return 0;
  }

  if (H == 1 && W == 1) {
    if (N == 0) {
      cout << 1 << endl;
    } else {
      cout << 0 << endl;
    }

    return 0;
  }

  if (H == 1) {
    int result = 0;
    int prev_c = -1;

    if (N == W) {
      cout << 0 << endl;
      return 0;
    } else {
      for (int i = 1; i < W + 1; ++i) {
        if (c[i] == 0) {
          if (i == 1) {
            ++result;
            prev_c = 0;
            continue;
          }

          if (prev_c == 1) {
            ++result;
          }
        }
        prev_c = c[i];
      }
    }

    cout << result << endl;
  }

  return 0;
}
