#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef unsigned long long ll;
using namespace std;

const ll MOD = 1000000007;
const ll MAX = 1000000;
vector<ll> fac(MAX, 0ll);
vector<ll> finv(MAX, 0ll);
vector<ll> inv(MAX, 0ll);

void COMinit() {
  fac[0] = fac[1] = 1;
  finv[0] = finv[1] = 1;
  inv[1] = 1;
  for (ll i = 2; i <= MAX; i++) {
    fac[i] = fac[i - 1] * i % MOD;
    inv[i] = MOD - inv[MOD % i] * (MOD / i) % MOD;
    finv[i] = finv[i - 1] * inv[i] % MOD;
  }
}

long long COM(int n, int k) {
  if (n < k)
    return 0;
  if (n < 0 || k < 0)
    return 0;
  // n! / (r!(n-r)!)
  return fac[n] * (finv[k] * finv[n - k] % MOD) % MOD;
}

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  ll X, Y, n, m;
  cin >> X >> Y;

  COMinit();

  // 2n + m = X
  // n + 2m = Y
  // <=> m = (2Y - X)/3, n = Y - 2m;
  if ((X + Y) % 3 == 0) {
    m = ((2 * Y - X) / 3);
    n = (Y - 2 * m);
    cout << COM(n + m, m) << endl;
  } else {
    cout << 0 << endl;
    return 0;
  }

  return 0;
}
