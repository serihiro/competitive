#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N, K, X, Y;
  cin >> N >> K >> X >> Y;
  int ans = 0;
  REPI(i, 1, N + 1) {
    if (i <= K) {
      ans += X;
    } else {
      ans += Y;
    }
  }
  cout << ans << endl;

  return 0;
}
