#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, n)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  string S;
  cin >> S;

  int ans = 0;
  int s_len = S.size();
  char last_op = '\0';

  for (int i = 0; i < s_len; ++i) {
    if (S.at(i) == '1') {
      if (last_op == '\0' || last_op == '+') {
        ++ans;
      } else {
        --ans;
      }
    } else {
      last_op = S.at(i);
    }
  }

  cout << ans << endl;

  return 0;
}
