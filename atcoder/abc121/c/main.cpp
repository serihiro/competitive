#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

ll A[100000];

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  ll N, M;
  cin >> N >> M;
  vector<pair<ll, ll>> cost(N);
  REP(i, N){
    cin >> cost[i].first >> cost[i].second;
  }
  sort(cost.begin(), cost.end());
  ll ans = 0;
  ll current_m = 0;
  REP(i, N){
    current_m += cost[i].second;
    ans += cost[i].second * cost[i].first;
    if(current_m >= M){
      ans -= cost[i].first * (current_m - M);
      break;
    }
  }

  cout << ans << endl;

  return 0;
}
