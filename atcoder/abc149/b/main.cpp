#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  ll ans_0, ans_1, A, B, K;
  cin >> A >> B >> K;
  ans_0 = A;
  ans_1 = B;

  ans_0 = max(A - K, 0ll);
  if(A < K){
    K -= A;
    ans_1 = max(B - K, 0ll);
  }

  cout << ans_0 << " " << ans_1 << endl;

  return 0;
}
