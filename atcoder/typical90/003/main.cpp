#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

vector<int> bfs(vector<vector<int>> g, int n, int s) {
  vector<int> dist(n, -1);
  queue<int> Q;
  Q.push(s);
  dist.at(s) = 0;

  while (!Q.empty()) {
    int v = Q.front();
    Q.pop();

    for (int nv : g.at(v)) {
      if (dist.at(nv) != -1) {
        continue;
      }

      dist.at(nv) = dist.at(v) + 1;
      Q.push(nv);
    }
  }

  return dist;
}

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;
  vector<vector<int>> G(N);

  REP(i, N - 1) {
    int a, b;
    cin >> a >> b;
    --a;
    --b;
    G.at(a).push_back(b);
    G.at(b).push_back(a);
  }

  vector<int> d1 = bfs(G, N, 0);
  auto itr = max_element(d1.begin(), d1.end());
  int max_index = distance(d1.begin(), itr);

  vector<int> d2 = bfs(G, N, max_index);

  cout << *max_element(d2.begin(), d2.end()) + 1 << endl;

  return 0;
}
