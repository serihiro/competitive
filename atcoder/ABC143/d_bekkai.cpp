#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  int L[2000];

  cin >> N;
  REP(i, N) { cin >> L[i]; }

  sort(L, L + N);

  int result = 0;
  int boundary;
  for (int i = 0; i < N; ++i) {
    for (int j = i + 1; j < N; ++j) {
      boundary = lower_bound(L, L + N, L[i] + L[j]) - L;
      result += max(boundary - (j + 1), 0);
      cout << boundary << ":" << boundary - (j + 1) << endl;
    }
  }

  cout << result << endl;

  return 0;
}
