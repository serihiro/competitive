// https://beta.atcoder.jp/contests/abc093

#include <iostream>
#include <algorithm>
#include <array>
using namespace std;

int main()
{
    cin.tie(0);
    ios::sync_with_stdio(false);

    array<int, 3> N = {};
    cin >> N[0] >> N[1] >> N[2];
    if (N[0] == N[1] && N[1] == N[2])
    {
        cout << 0 << endl;
        return 0;
    }

    sort(N.begin(), N.end());

    int diff_0 = N[2] - N[0];
    int diff_1 = N[2] - N[1];
    if ((diff_0 + diff_1) % 2 == 0)
    {
        cout << (diff_0 + diff_1) / 2 << endl;
    }
    else
    {
        cout << (diff_0 + diff_1) / 2 + 2 << endl;
    }

    return 0;
}
