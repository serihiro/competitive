#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  string str;
  int q;
  cin >> str >> q;
  int a, b;
  string p;
  REP(i, q) {
    string f;
    cin >> f;
    cin >> a >> b;
    if (f == "print") {
      cout << str.substr(a, b - a + 1) << endl;
    } else if (f == "reverse") {
      reverse(str.begin() + a, str.begin() + b + 1);
    } else { // replace
      cin >> p;
      REP(i, p.length()) { str.at(i + a) = p.at(i); }
    }
  }

  return 0;
}
