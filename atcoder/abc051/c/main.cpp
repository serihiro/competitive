#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

void trace(int x0, int y0, int x1, int y1);

void trace(int x0, int y0, int x1, int y1) {
  // cout << x0 << "/" << y0 << "=>" << x1 << "/" << y1 << endl;

  if (x1 - x0 > 0) {
    for (int i = x0; i < x1; ++i) {
      cout << "R";
    }
  } else {
    for (int i = x0; i > x1; --i) {
      cout << "L";
    }
  }

  if (y1 - y0 > 0) {
    for (int i = y0; i < y1; ++i) {
      cout << "U";
    }
  } else {
    for (int i = y0; i > y1; --i) {
      cout << "D";
    }
  }
}

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int sx, sy, tx, ty;
  int cx, cy, nx, ny;
  cin >> sx >> sy >> tx >> ty;

  trace(sx, sy, tx, ty);
  trace(tx, ty, sx, sy);

  if (tx > sx) {
    // 右上
    if (ty > tx) {
      cx = sx;
      cy = sy;
      nx = cx;
      ny = cy - 1;
      trace(cx, cy, nx, ny);
      cx = nx;
      cy = ny;
      nx += abs(tx - sx) + 1;
      ny = cy;
      trace(cx, cy, nx, ny);
      cx = nx;
      cy = ny;
      nx = cx;
      ny += abs(ty - sy) + 1;
      trace(cx, cy, nx, ny);
      cx = nx;
      cy = ny;
      nx = cx - 1;
      ny = cy;
      trace(cx, cy, nx, ny);
      cx = nx;
      cy = ny;
      nx = cx;
      ny = cy + 1;
      trace(cx, cy, nx, ny);
      cx = nx;
      cy = ny;
      nx -= abs(tx - sx) + 1;
      ny = cy;
      trace(cx, cy, nx, ny);
      cx = nx;
      cy = ny;
      nx = cx;
      ny -= abs(ty - sy) + 1;
      trace(cx, cy, nx, ny);
      cx = nx;
      cy = ny;
      nx += 1;
      ny = cy;
      trace(cx, cy, nx, ny);
      // 右下
    } else {
      cx = sx;
      cy = sy;
      nx = cx;
      ny = cy + 1;
      trace(cx, cy, nx, ny);
      cx = nx;
      cy = ny;
      nx += abs(tx - sx) + 1;
      ny = cy;
      trace(cx, cy, nx, ny);
      cx = nx;
      cy = ny;
      nx = cx;
      ny -= abs(ty - sy) + 1;
      trace(cx, cy, nx, ny);
      cx = nx;
      cy = ny;
      nx = cx - 1;
      ny = cy;
      trace(cx, cy, nx, ny);
      cx = nx;
      cy = ny;
      nx = cx;
      ny = cy - 1;
      trace(cx, cy, nx, ny);
      cx = nx;
      cy = ny;
      nx -= abs(tx - sx) + 1;
      ny = cy;
      trace(cx, cy, nx, ny);
      cx = nx;
      cy = ny;
      nx = cx;
      ny += abs(ty - sy) + 1;
      trace(cx, cy, nx, ny);
      cx = nx;
      cy = ny;
      nx += 1;
      ny = cy;
      trace(cx, cy, nx, ny);
    }
  } else {
    // 左上
    if (ty > tx) {

      // 左下
    } else {
    }
  }

  return 0;
}
