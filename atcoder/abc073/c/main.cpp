#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;
  unordered_map<ll, int> count;
  ll a;
  REP(i, N) {
    cin >> a;
    if (count.find(a) == count.end()) {
      count.insert(make_pair(a, 1));
    } else {
      ++count.at(a);
    }
  }

  int ans = 0;
  for (auto const &it : count) {
    if (it.second % 2 == 1) {
      ++ans;
    }
  }

  cout << ans << endl;

  return 0;
}
