#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;
  unordered_map<ll, int> count;
  ll a;
  REP(i, N) {
    cin >> a;
    if (count.find(a) == count.end()) {
      count.insert(make_pair(a, 1));
    } else {
      ++count.at(a);
    }
  }

  vector<ll> availables;
  for (const auto &it : count) {
    if (it.second >= 2) {
      availables.push_back(it.first);
    }
  }

  if (availables.size() < 2) {
    cout << 0 << endl;
    return 0;
  }

  sort(availables.begin(), availables.end(), greater<ll>());

  ll ans = 0;
  ll longest = availables.at(0);
  for (const ll &v : availables) {
    if (count.at(v) >= 4) {
      ans = max(ans, v * v);
    }
    if (v != longest) {
      ans = max(ans, longest * v);
    }
  }

  cout << ans << endl;

  return 0;
}
