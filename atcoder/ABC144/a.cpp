#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int A, B;
  cin >> A >> B;

  if ((1 <= A && A <= 9) && (1 <= B && B <= 9)) {
    cout << A * B << endl;
  } else {
    cout << -1 << endl;
  }

  return 0;
}
