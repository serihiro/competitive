#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int A, B, C, D;
  cin >> A >> B >> C >> D;
  bool takahashi = true;
  while (true) {
    if (takahashi) {
      takahashi = false;
      C -= B;
      if (C <= 0) {
        cout << "Yes" << endl;
        return 0;
      }
    } else {
      takahashi = true;
      A -= D;
      if (A <= 0) {
        cout << "No" << endl;
        return 0;
      }
    }
  }

  return 0;
}
