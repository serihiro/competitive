#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;
  vector<ll> A(N, 0);
  REP(i, N) { cin >> A.at(i); }

  REPI(i, 1, N) {
    if (A.at(i - 1) == A.at(i)) {
      cout << "stay" << endl;
    } else if (A.at(i - 1) > A.at(i)) {
      cout << "down " << A.at(i - 1) - A.at(i) << endl;
    } else {
      cout << "up " << A.at(i) - A.at(i - 1) << endl;
    }
  }

  return 0;
}
