#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;
  vector<int> P(N);
  REP(i, N) { cin >> P.at(i); }
  vector<int> P2(P);
  sort(P2.begin(), P2.end());
  int diff = 0;
  REP(i, N) {
    if (P.at(i) != P2.at(i)) {
      ++diff;
    }
  }
  if (diff == 0 || diff == 2) {
    cout << "YES" << endl;
  } else {
    cout << "NO" << endl;
  }

  return 0;
}
