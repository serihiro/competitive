
#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, n)
#define REPI(i,a,b) for(int i=int(a);i<int(b);++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N, K;
  string S;

  cin >> N >> K >> S;

  int ans = 0;
  REPI(i, 1, N){
    if(S[i-1] == S[i]){
      ++ans;
    }
  }

  cout << min(ans + 2 * K, N-1) << endl;

  return 0;
}
