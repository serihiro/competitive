#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  string S;
  cin >> S;

  REP(i, 19) {
    if (S[i] == ',') {
      cout << " ";
    } else {
      cout << S[i];
    }
  }

  cout << endl;

  return 0;
}
