#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N, K;
  cin >> N >> K;
  vector<ll> H(N);
  REP(i, N) { cin >> H[i]; }
  sort(H.begin(), H.end());
  ll ans = 0;
  REP(i, N - K) { ans += H[i]; }
  cout << ans << endl;

  return 0;
}
