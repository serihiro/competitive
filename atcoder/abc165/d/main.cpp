#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  long double A, B, N;
  cin >> A >> B >> N;
  // long double ans = 0;
  long double a_b = A / B;

  // long double prev_ans = -1;
  // for (long double x = 1.0; x <= N; x += 1.0) {
  //   ans = max(ans, floor(a_b * x) - A * floor(x / B));
  //   // if (prev_ans != -1) {
  //   //   if (prev_ans > ans) {
  //   //     cout << ans << endl;
  //   //     return 0;
  //   //   }
  //   // }
  //   prev_ans = ans;

  //   cout << setprecision(5) << floor(a_b * x) << ":" << A * floor(x / B)
  //        << endl;
  // }

  long double x = min(N, B - 1);
  cout << floor(a_b * x) - A * floor(x / B) << endl;

  return 0;
}
