#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int judge_colour(int i) {
  int result;
  if (1 <= i && i <= 399) {
    result = 0;
  } else if (400 <= i && i <= 799) {
    result = 1;
  } else if (800 <= i && i <= 1199) {
    result = 2;
  } else if (1200 <= i && i <= 1599) {
    result = 3;
  } else if (1600 <= i && i <= 1999) {
    result = 4;
  } else if (2000 <= i && i <= 2399) {
    result = 5;
  } else if (2400 <= i && i <= 2799) {
    result = 6;
  } else if (2800 <= i && i <= 3199) {
    result = 7;
  } else {
    result = 8;
  }

  return result;
}

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;
  int a;
  set<int> variations;
  int count_free = 0;
  REP(i, N) {
    cin >> a;
    int c = judge_colour(a);
    if (c == 8)
      ++count_free;
    variations.insert(c);
  }

  if (count_free > 0) {
    int min_size = max((int)(variations.size() - 1), 1);
    int max_size = variations.size() - 1 + count_free;

    cout << min_size << " " << max_size << endl;
  } else {
    cout << variations.size() << " " << variations.size() << endl;
  }

  return 0;
}
