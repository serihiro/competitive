C = []
for _ in range(3):
    C.append(list(map(int, input().split())))

c01 = (C[0][1] - C[0][0]) == (C[1][1] - C[1][0]) and (C[1][1] - C[1][0]) == (
    C[2][1] - C[2][0]
)
c12 = (C[0][2] - C[0][1]) == (C[1][2] - C[1][1]) and (C[1][2] - C[1][1]) == (
    C[2][2] - C[2][1]
)
c02 = (C[0][2] - C[0][0]) == (C[1][2] - C[1][0]) and (C[1][2] - C[1][0]) == (
    C[2][2] - C[2][0]
)

r01 = (C[1][0] - C[0][0]) == (C[1][1] - C[0][1]) and (C[1][1] - C[0][1]) == (
    C[1][2] - C[0][2]
)
r12 = (C[2][0] - C[1][0]) == (C[2][1] - C[1][1]) and (C[2][1] - C[1][1]) == (
    C[2][2] - C[1][2]
)
r02 = (C[2][0] - C[0][0]) == (C[2][1] - C[0][1]) == (C[2][1] - C[0][1]) and (
    C[2][1] - C[0][1]
) == (C[2][2] - C[0][2])

# print(c01, c12, c02, r01, r12, r02)

if c01 and c12 and c02 and r01 and r12 and r02:
    print("Yes")
else:
    print("No")
