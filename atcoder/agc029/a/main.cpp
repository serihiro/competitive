#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  string S;
  cin >> S;
  int l = S.length();
  ll ans = 0;
  ll r_black = l;
  for (ll i = l - 1; i >= 0; --i) {
    // cout << i << ":" << S.at(i) << ":" << r_black << endl;

    if (S.at(i) == 'B') {
      ans += r_black - i - 1;
      r_black = i + (r_black - i - 1);
    }
  }

  cout << ans << endl;

  return 0;
}
