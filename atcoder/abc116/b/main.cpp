#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int f(int n) {
  if (n % 2 == 0) {
    return n / 2;
  } else {
    return 3 * n + 1;
  }
}

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int s;
  cin >> s;

  int a_n, a_n_p;
  a_n_p = s;
  vector<bool> count(1000001, false);
  count.at(s) = true;

  REPI(i, 2, 1000001) {
    a_n = f(a_n_p);
    if (count.at(a_n)) {
      cout << i << endl;
      break;
    }

    count.at(a_n) = true;
    a_n_p = a_n;
  }

  return 0;
}
