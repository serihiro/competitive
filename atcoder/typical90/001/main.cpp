#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

bool solve(const vector<ll> &a, const ll &mid, const ll &n, const ll &k,
           const ll &l) {
  ll count = 0, pre = 0;
  REP(i, n) {
    if (a.at(i) - pre >= mid && l - a.at(i) >= mid) {
      ++count;
      pre = a[i];
    }
  }

  if (count >= k) {
    return true;
  }

  return false;
}

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  ll N, L, K;
  cin >> N >> L >> K;
  vector<ll> A(N);
  REP(i, N) { cin >> A.at(i); }

  ll ok = 0;
  ll ng = L;
  while (abs(ok - ng) > 1) {
    ll mid = (ok + ng) / 2;
    if (solve(A, mid, N, K, L)) {
      ok = mid;
    } else {
      ng = mid;
    }
  }

  cout << ok << endl;

  return 0;
}
