#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N, M;
  ll A[100010];

  cin >> N >> M;
  REP(i, N) {cin >> A[i];}
  sort(A, A+N);

  int border_line = 1;
  while(border_line*border_line+border_line < 2*M){
    ++border_line;
  }

  cout << border_line << endl;
  
  for(int i = 1; i < N-1; ++i){
    A[N-1-i] = ceil(A[N-1-i] / pow(2, (border_line-i)));
    cout << N-1-i << ":" << A[N-1-i] << endl;
  }
  A[N-1] = ceil(A[N-1] / pow(2, M - ((border_line-1) + (border_line-1) * (border_line-1))));
  cout << N-1 << "::" << A[N-1] << endl;


  ll result = 0;
  REP(i, N) {result += A[i];}

  cout << result;

  return 0;
}
