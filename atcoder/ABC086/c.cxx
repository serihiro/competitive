// https://abc086.contest.atcoder.jp/tasks/arc089_a

#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

#define REP(i, n) for (int i = 0; i < (n); ++i)

int main()
{
    cin.tie(0);
    ios::sync_with_stdio(false);

    int n;
    cin >> n;

    int i = 0;
    int ts, tx, ty;
    int cs, cx, cy;
    cs = 1;
    cx = 0;
    cy = 0;
    while (i < n && cin >> ts >> tx >> ty)
    {
        // printf("ts: %d, cs: %d, tx: %d, ty: %d\n", ts, cs, tx, ty);

        while (cs <= ts)
        {
            if (tx > cx)
            {
                cx += 1;
            }
            else if (tx < cx)
            {
                cx -= 1;
            }
            else if (ty > cy)
            {
                cy += 1;
            }
            else if (ty < cy)
            {
                cy -= 1;
            }
            else
            { // (cx,cy) == (tx, ty)
                cx++;
            }

            // printf("cx: %d, cy: %d\n", cx, cy);
            ++cs;
        }

        if (cx == tx && cy == ty)
        {
            ++i;
        }
        else
        {
            // printf("cx: %d, cy: %d\n", cx, cy);
            cout << "No" << endl;
            return 0;
        }
    }
    cout << "Yes" << endl;

    return 0;
}
