#include <iostream>
using namespace std;

int main()
{
    cin.tie(0);
    ios::sync_with_stdio(false);

    long long x;
    cin >> x;

    if (x < 7)
    {
        cout << 1 << endl;
        return 0;
    }

    long long count = 0;
    count = x % 11;

    if (count == 0)
    {
        cout << x / 11 * 2 << endl;
    }
    else
    {
        if (count < 7)
        {
            cout << x / 11 * 2 + 1 << endl;
        }
        else
        {
            cout << x / 11 * 2 + 2 << endl;
        }
    }

    return 0;
}

// int main(){
//     cin.tie(0);
//     ios::sync_with_stdio(false);

//     long x;
//     cin >> x;

//     if (x < 7) {
//         cout << 1 << endl;
//         return 0;
//     }

//     long current = 0;
//     long count = 0;
//     count = x / 11 * 2;
//     current = x % 11;

//     if (0 < current && current < 7) {
//         ++count;
//     }else if( 6 < current ) {
//         count += 2;
//     }

//     cout << count << endl;

//     return 0;
// }
