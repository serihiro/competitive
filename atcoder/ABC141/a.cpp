// 2019年 11月 5日 火曜日 17時06分51秒 JST

#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  string S;
  cin >> S;

  if(S == "Sunny"){
    cout << "Cloudy" << endl;
  }else if(S == "Cloudy") {
    cout << "Rainy" << endl;
  }else{
    cout << "Sunny" << endl;
  }
  
  return 0;
}
