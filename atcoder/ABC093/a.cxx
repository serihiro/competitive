// https://beta.atcoder.jp/contests/abc093

#include <iostream>
#include <algorithm>
using namespace std;

int main()
{
    cin.tie(0);
    ios::sync_with_stdio(false);

    string s;
    cin >> s;
    sort(s.begin(), s.end());
    if (s[0] == 'a' && s[1] == 'b' && s[2] == 'c')
    {
        cout << "Yes" << endl;
    }
    else
    {
        cout << "No" << endl;
    }

    return 0;
}
