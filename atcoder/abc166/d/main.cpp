#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  ll X;
  cin >> X;

  vector<ll> A(1100), B(1100);
  REP(i, 1001) { A.at(i) = B.at(i) = pow(i, 5); }

  REP(i, 1001) {
    REP(j, 1001) {
      // cout << i << "/" << j << endl;
      // cout << A.at(i) - B.at(j) << ":" << (-1) * A.at(i) - B.at(j) << ":"
      //      << A.at(i) + B.at(j) << endl;
      if (A.at(i) - B.at(j) == X) {
        cout << i << " " << j << endl;
        return 0;
      } else if ((-1) * A.at(i) - B.at(j) == X) {
        cout << -i << " " << j << endl;
        return 0;
      } else if (A.at(i) + B.at(j) == X) {
        cout << i << " " << -j << endl;
        return 0;
      }
    }
  }

  return 0;
}
