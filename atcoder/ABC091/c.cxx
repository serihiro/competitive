// https://beta.atcoder.jp/contests/abc091
// giveup

#include <iostream>
#include <algorithm>
using namespace std;

int main()
{
    cin.tie(0);
    ios::sync_with_stdio(false);

    int N, tmp_a, tmp_b, tmp_c, tmp_d;
    int a[100] = {};
    int b[100] = {};
    int c[100] = {};
    int d[100] = {};

    cin >> N;

    for (int i = 0; i < N; ++i)
    {
        cin >> a[i] >> b[i];
    }

    for (int i = 0; i < N; ++i)
    {
        cin >> c[i] >> d[i];
    }

    int ans = 0;

    for (int i = 0; i < N; ++i)
    {
        int cnt = 0;
        for (int j = i; j < N; ++j)
        {
            if (a[i] < c[i] && b[j] < d[j])
            {
                ++cnt;
            }
        }
        ans = max(ans, cnt);
    }

    return 0;
}