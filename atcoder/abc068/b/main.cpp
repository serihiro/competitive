#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;
  if (N == 1) {
    cout << 1 << endl;
    return 0;
  }

  int ans = 2;
  int cnt = 1;
  while (N >= pow(2, cnt)) {
    ans = pow(2, cnt);
    ++cnt;
  }
  cout << ans << endl;

  return 0;
}
