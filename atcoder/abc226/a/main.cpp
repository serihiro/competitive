#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  string X;
  cin >> X;
  REP(i, (int)X.size()) {
    if (X.at(i) == '.') {
      int n = int(X.at(i + 1) - '0');
      X.at(i + 1) = '0';
      cout << X << endl;
      if (5 <= n) {
        X.at(i - 1) = char((X.at(i - 1)) - '0' + 1);
      }
      break;
    }
  }

  cout << X << endl;
  // for (const char &c : X) {
  //   if (c == '.') {
  //     break;
  //   }
  //   cout << c;
  // }
  // cout << endl;

  return 0;
}
