#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int K, N;
  cin >> N >> K;
  string S;
  cin >> S;

  vector<vector<int>> nex(N + 1, vector<int>(26, N));
  for (int i = N - 1; i >= 0; --i) {
    nex.at(i) = nex.at(i + 1);
    nex.at(i).at(S.at(i) - 'a') = i;
  }

  string ans = "";
  int j = -1;
  REP(i, K) {
    for (char c = 'a'; c <= 'z'; ++c) {
      int k = nex.at(j + 1).at(c - 'a');
      int requirement = K - i;
      if (N - k >= requirement) {
        ans += c;
        j = k;
        break;
      }
    }
  }

  cout << ans << endl;

  return 0;
}
