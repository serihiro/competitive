#include <iostream>
#include <vector>
#include <algorithm> // algorithm header is required at AOJ
using namespace std;

#define REP(i, n) for (int i = 0; i < (n); ++i)

int main()
{
    cin.tie(0);
    ios::sync_with_stdio(false);

    int n;
    cin >> n;

    vector<int> arr;
    int i;
    int c = 0;
    while (c < n && cin >> i)
    {
        arr.push_back(i);
        ++c;
    }

    reverse(arr.begin(), arr.end());

    REP(i2, n)
    {
        if (i2 > 0)
        {
            cout << " ";
        }

        cout << arr[i2];
    }

    cout << endl;

    return 0;
}
