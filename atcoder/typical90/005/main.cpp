#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  ll MOD = 1e9 + 7;

  ll N, B, K;
  cin >> N >> B >> K;

  vector<int> C(K);
  REP(i, K) { cin >> C.at(i); }
  vector<vector<int>> DP(N + 1, vector<int>(B + 1, 0));
  // １個も選んでない状態ではどうやってもmod Bは0にしかならない．
  DP.at(0).at(0) = 1;

  REP(i, N) {
    REP(j, B + 1) {
      REP(k, K) {
        int next = (10 * j + C.at(k)) % B;
        DP.at(i + 1).at(next) += DP.at(i).at(j);
        DP.at(i + 1).at(next) %= MOD;
      }
    }
  }

  // REP(i, N + 1) {
  //   REP(j, B + 1) { cout << DP.at(i).at(j) << " "; }
  //   cout << endl;
  // }

  cout << DP.at(N).at(0) << endl;

  return 0;
}
