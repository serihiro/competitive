#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N, L;
  cin >> N >> L;
  string s;
  vector<string> S;
  REP(i, N) {
    cin >> s;
    S.push_back(s);
  }

  sort(S.begin(), S.end());
  REP(i, N) { cout << S.at(i); }
  cout << endl;

  return 0;
}
