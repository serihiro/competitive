#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int H, W;
  while (cin >> H >> W) {
    if (H == 0 && W == 0) {
      return 0;
    }

    REP(i, H) {
      REP(j, W) {
        if (i % 2 == 0) {
          if (j % 2 == 0) {
            cout << "#";
          } else {
            cout << ".";
          }
        } else {
          if (j % 2 == 0) {
            cout << ".";
          } else {
            cout << "#";
          }
        }
      }
      cout << endl;
    }
    cout << endl;
  }

  return 0;
}
