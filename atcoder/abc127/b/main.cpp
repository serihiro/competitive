#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int R, D, X;
  cin >> R >> D >> X;

  int x = X;
  REP(i, 10) {
    cout << R * x - D << endl;
    x = R * x - D;
  }

  return 0;
}
