#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

ll f(ll a) {
  ll ans = 0;
  while (a > 0 && a % 2 == 0) {
    a /= 2;
    ++ans;
  }
  return ans;
}

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;
  ll a;
  ll ans = 0ll;
  REP(i, N) {
    cin >> a;
    if (a % 2 == 0) {
      ans += f(a);
    }
  }
  cout << ans << endl;

  return 0;
}
