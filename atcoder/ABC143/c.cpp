#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  string S;

  cin >> N >> S;

  if (N == 1) {
    cout << 1 << endl;
    return 0;
  }

  int result = 1;
  char prev = '\0';

  REP(i, N) {
    if (prev == '\0') {
      prev = S[i];
    }

    if (S[i] != prev) {
      ++result;
      prev = S[i];
    }
  }

  cout << result << endl;

  return 0;
}
