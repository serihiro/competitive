#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N, K;
  cin >> N >> K;
  int d;

  vector<int> count(N, 0);

  REP(i, K) {
    cin >> d;
    int a;
    REP(j, d) {
      cin >> a;
      --a;
      ++count.at(a);
    }
  }

  int ans = 0;
  REP(i, N) {
    if (count.at(i) == 0) {
      ++ans;
    }
  }

  cout << ans << endl;

  return 0;
}
