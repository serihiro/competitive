#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;
  vector<vector<int>> A(2, vector<int>(N, 0));
  REP(i, 2) {
    REP(j, N) { cin >> A.at(i).at(j); }
  }

  int ans = 0;

  REP(i, N) {
    int tmp_ans = A.at(0).at(0);
    int r = 0;
    REP(j, N) {
      if (j > 0) {
        tmp_ans += A.at(r).at(j);
      }
      if (i == j) {
        ++r;
        tmp_ans += A.at(r).at(j);
      }
    }
    ans = max(ans, tmp_ans);
  }
  cout << ans << endl;

  return 0;
}
