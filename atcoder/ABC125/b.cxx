#include <iostream>
#include <algorithm>

using namespace std;
typedef long long ll;

int N;
int V[20];
int C[20];
int DIFF[20];

int main()
{
    cin.tie(0);
    ios::sync_with_stdio(false);

    cin >> N;
    for (int i = 0; i < N; ++i)
        cin >> V[i];
    for (int i = 0; i < N; ++i)
    {
        cin >> C[i];
        DIFF[i] = V[i] - C[i];
    }

    sort(DIFF, DIFF + N, greater<int>());
    int result = 0;
    for (int i = 0; i < N; ++i)
    {
        if (DIFF[i] < 0)
            break;
        result += DIFF[i];
    }

    cout << result << endl;

    return 0;
}
