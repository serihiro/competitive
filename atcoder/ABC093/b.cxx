// https://beta.atcoder.jp/contests/abc093

#include <iostream>
using namespace std;

int main()
{
    cin.tie(0);
    ios::sync_with_stdio(false);

    long long A, B;
    int K;

    cin >> A >> B >> K;

    long long show = A;

    for (int i = 1; i <= K && show <= B; ++i)
    {
        cout << show << endl;
        show += 1;
    }

    if (B - K + 1 > show)
    {
        show = B - K + 1;
    }

    int max = B - show + 1;
    for (int i = 1; i <= max && show <= B; ++i)
    {
        cout << show << endl;
        show += 1;
    }

    return 0;
}
