#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;
  vector<int> A(N);

  int alice = 0;
  int bob = 0;
  REP(i, N) { cin >> A.at(i); }

  sort(A.begin(), A.end(), greater<int>());

  REP(i, N) {
    if (i % 2 == 0) {
      alice += A.at(i);
    } else {
      bob += A.at(i);
    }
  }
  cout << alice - bob << endl;

  return 0;
}
