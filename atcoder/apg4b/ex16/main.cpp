#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, n)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int b_a = -1;
  int c_a = 0;
  for (int i = 0; i < 5; ++i) {
    cin >> c_a;
    if (b_a == c_a) {
      cout << "YES" << endl;
      return 0;
    }
    b_a = c_a;
  }

  cout << "NO" << endl;

  return 0;
}
