#include <algorithm>
#include <cmath>
#include <iostream>

using namespace std;

typedef long long ll;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  ll N, M;
  cin >> N >> M;

  // N > 2 && M > 2
  cout << abs((M - 2) * (N - 2)) << endl;

  return 0;
}