#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  string S;
  cin >> N >> S;
  int ans = 0;
  int x = 0;
  int l = S.length();
  REP(i, l) {
    if (S.at(i) == 'I') {
      ++x;
    } else {
      --x;
    }
    ans = max(ans, x);
  }

  cout << ans << endl;

  return 0;
}
