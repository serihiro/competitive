#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  string S;
  cin >> S;

  bool result = true;
  // odd
  for(int i=0; i < S.length(); i+=2){
    if(!(S[i] == 'R' || S[i] == 'U' || S[i] == 'D')){
      result = false;
    }
  }

  if(!result){
    cout << "No" << endl;
    return 0;
  }

  if(S.length() == 1){
    cout << "Yes" << endl;
    return 0;
  }

  // even
  for(int i=1; i < S.length(); i+=2){
    if(!(S[i] == 'L' || S[i] == 'U' || S[i] == 'D')){
      result = false;
    }
  }

  if(result){
    cout << "Yes" << endl;
  }else{
    cout << "No" << endl;
  }

  return 0;
}
