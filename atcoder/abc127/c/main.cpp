#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N, M;
  cin >> N >> M;
  int l, m, left, right;
  left = 1 << 31;
  right = 1 << 29;
  REP(i, M) {
    cin >> l >> m;
    left = max(left, l);
    right = min(right, m);
  }

  if (right < left) {
    cout << 0 << endl;
  } else {
    cout << right - left + 1 << endl;
  }

  return 0;
}
