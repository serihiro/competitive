#include <iostream>
#include <algorithm>
#include <cmath>

using namespace std;
typedef long long ll;
int A, B, T;

int main()
{
    cin.tie(0);
    ios::sync_with_stdio(false);

    cin >> A >> B >> T;

    cout << floor((T + 0.5) / A) * B << endl;

    return 0;
}
