#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  string S;
  cin >> S;
  for (char c : S) {
    if (!('0' <= c && c <= '9')) {
      cout << "error" << endl;
      return 0;
    }
  }
  cout << stoi(S) * 2 << endl;

  return 0;
}
