#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  string w;
  cin >> w;
  vector<int> count(26, 0);
  int l = w.length();
  REP(i, l) { ++count.at(w.at(i) - 'a'); }

  REP(i, 26) {
    if (count.at(i) == 0) {
      continue;
    }

    if (count.at(i) % 2 != 0) {
      cout << "No" << endl;
      return 0;
    }
  }

  cout << "Yes" << endl;

  return 0;
}
