#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  ll H, N;
  cin >> H >> N;
  vector<int> A(N);
  REP(i, N) { cin >> A[i]; }
  sort(A.begin(), A.end());
  ll total = 0;
  REP(i, N) {
    total += A[i];
    if (total >= H) {
      cout << "Yes" << endl;
      return 0;
    }
  }

  cout << "No" << endl;
  return 0;
}
