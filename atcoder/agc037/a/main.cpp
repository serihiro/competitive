#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  string S;
  cin >> S;

  int l = S.length();
  int ans = l;
  REP(i, l - 1) {
    if (S.at(i) == S.at(i + 1)) {
      --ans;
      i += 2;
    }
  }

  cout << ans << endl;

  return 0;
}
