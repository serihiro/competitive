#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N, M;
  cin >> N >> M;
  vector<int> A(N), B(N);
  REP(i, N) { cin >> A.at(i) >> B.at(i); }
  vector<int> C(M), D(M);
  REP(i, M) { cin >> C.at(i) >> D.at(i); }

  REP(i, N) {
    ll c = 100;
    ll dist = 1 << 29;

    for (ll j = 0; j < M; ++j) {
      ll d = abs(C.at(j) - A.at(i)) + abs(D.at(j) - B.at(i));
      // cout << i << ":" << j + 1 << ":" << d << endl;
      if (dist > d) {
        dist = d;
        c = j + 1;
      } else if (dist == d) {
        c = min(c, j + 1);
      }
    }

    cout << c << endl;
  }

  return 0;
}
