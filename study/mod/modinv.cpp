#include <iostream>
using namespace std;

typedef long long ll;

ll modinv(ll a, ll m) {
  ll b = m;
  ll u = 1;
  ll v = 0;
  ll t = a / b;

  while (b) {
    t = a / b;
    a -= t * b;
    swap(a, b);
    u -= t * v;
    swap(u, v);
  }
  u %= m;
  if (u < 0)
    u += m;

  return u;
}

int main() {
  for (int i = 1; i < 13; ++i) {
    cout << i << " 's inv: " << modinv(i, 13)
         << ", i * i^-1 (mod 13) = " << i * modinv(i, 13) % 13 << endl;
  }
}
