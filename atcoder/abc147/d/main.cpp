#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

const ll MOD = 1e9 + 7;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;
  vector<ll> A(N);
  REP(i, N) { cin >> A[i]; }

  ll ans = 0, now = 0;
  ll x = 0;

  REP(i, 60) {
    x = 0;
    REP(j, N) {
      if (A[j] >> i & 1) {
        ++x;
      }
    }
    now = x * (N - x) % MOD;
    REP(j, i) { now = now * 2 % MOD; }
    ans = ans + now;
    ans = ans % MOD;
  }

  cout << ans << endl;
  return 0;
}