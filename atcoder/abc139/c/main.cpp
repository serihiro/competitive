#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;
  int ans = 0;
  ll h_0;
  ll h_1;
  int count = 0;
  cin >> h_0;
  REPI(i, 1, N) {
    cin >> h_1;
    if (h_1 <= h_0) {
      ++count;
    } else {
      ans = max(ans, count);
      count = 0;
    }
    h_0 = h_1;
  }
  if (count != 0) {
    ans = max(ans, count);
  }

  cout << ans << endl;

  return 0;
}
