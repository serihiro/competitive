#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  vector<vector<vector<int>>> m(4, vector<vector<int>>(3, vector<int>(10, 0)));
  int n;
  cin >> n;

  int b, f, r, v;
  REP(i, n) {
    cin >> b >> f >> r >> v;
    --b;
    --f;
    --r;
    m.at(b).at(f).at(r) += v;
  }

  REP(i, 4) {
    REP(j, 3) {
      REP(k, 10) { cout << " " << m.at(i).at(j).at(k); }
      cout << endl;
    }
    if (i != 3) {
      cout << "####################" << endl;
    }
  }

  return 0;
}
