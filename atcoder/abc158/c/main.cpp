#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int A, B;
  cin >> A >> B;
  int c_a = ceil(A / 0.08);
  int c_b = ceil(B / 0.10);
  int c = max(c_a, c_b);

  if (floor(c * 0.08) == A && floor(c * 0.10) == B) {
    cout << c << endl;
  } else {
    cout << -1 << endl;
  }

  return 0;
}
