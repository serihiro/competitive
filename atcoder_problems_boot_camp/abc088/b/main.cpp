#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;
  vector<int> A(N);
  REP(i, N) { cin >> A.at(i); }
  sort(A.begin(), A.end(), greater<int>());
  int a = 0, b = 0;
  for (int i = 0; i < N; i += 2) {
    a += A.at(i);
    if (i + 1 < N) {
      b += A.at(i + 1);
    }
  }

  cout << a - b << endl;

  return 0;
}
