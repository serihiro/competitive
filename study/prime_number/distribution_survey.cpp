#include <bits/stdc++.h>

using namespace std;

bool is_prime(int n) {
  if (n < 4) {
    return true;
  }

  for (int i = 2; i * i <= n; ++i) {
    if (n % i == 0) {
      return false;
    }
  }

  return true;
}

int main() {
  int seq = 1;
  int last_prime_number = -1;
  for (int i = 2; i <= 1000000; ++i) {
    if (is_prime(i)) {
      if (last_prime_number != -1) {
        cout << seq << ',' << i - last_prime_number << endl;
      } else {
        cout << seq << ',' << i << endl;
      }
      ++seq;
      last_prime_number = i;
    }
  }

  return 0;
}
