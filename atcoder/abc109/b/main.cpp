#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;
  vector<string> count;
  string w, p_w;
  cin >> p_w;
  count.push_back(p_w);
  REPI(i, 1, N) {
    cin >> w;
    if (p_w.back() != w.front()) {
      cout << "No" << endl;
      return 0;
    }

    if (find(count.begin(), count.end(), w) != count.end()) {
      cout << "No" << endl;
      return 0;
    }

    count.push_back(w);
    p_w = w;
  }

  cout << "Yes" << endl;

  return 0;
}
