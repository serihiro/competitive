#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

ll f(ll h) {
  if (h == 1) {
    return 1;
  }
  return 1 + f(h / 2);
}

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  ll H;
  cin >> H;
  if (H == 1) {
    cout << 1 << endl;
    return 0;
  }

  ll depth = f(H);
  ll ans = 0;
  REPI(i, 1, depth + 1) { ans += pow(2, i - 1); }
  cout << ans << endl;

  return 0;
}
