#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  double tmp_a;
  double result = 0;

  cin >> N;
  REP(i, N) { 
    cin >> tmp_a; 
    result += 1 / tmp_a;
  }

  cout << setprecision(10) << 1/result << endl;

  return 0;
}
