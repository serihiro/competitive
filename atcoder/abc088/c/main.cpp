#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  vector<vector<int>> C(3);
  int c;
  REP(i, 3) {
    REP(j, 3) {
      cin >> c;
      C.at(i).push_back(c);
    }
  }

  bool ans = true;
  REP(j, 2) {
    if (!(C.at(0).at(j + 1) - C.at(0).at(j) ==
              C.at(1).at(j + 1) - C.at(1).at(j) &&
          C.at(1).at(j + 1) - C.at(1).at(j) ==
              C.at(2).at(j + 1) - C.at(2).at(j))) {
      ans = false;
      break;
    }
  }

  if (!ans) {
    cout << "No" << endl;
    return 0;
  }

  REP(i, 2) {
    if (!(C.at(i + 1).at(0) - C.at(i).at(0) ==
              C.at(i + 1).at(1) - C.at(i).at(1) &&
          C.at(i + 1).at(1) - C.at(i).at(1) ==
              C.at(i + 1).at(2) - C.at(i).at(2))) {
      ans = false;
      break;
    }
  }

  if (!ans) {
    cout << "No" << endl;
  } else {
    cout << "Yes" << endl;
  }

  return 0;
}
