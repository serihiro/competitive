A, B = map(int, input().split())
if B == 1:
    print(0)
    exit(0)

result = 1
total = A
while total < B:
    total += A - 1
    result += 1
print(result)
