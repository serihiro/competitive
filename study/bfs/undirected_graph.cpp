#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;
using Graph = vector<vector<int>>; // G[E][V]

int main() {
  int N; // N is the size of the vertexes.
  int M; // M is the size of the edges.
  cin >> N >> M;

  Graph G(N);
  REP(i, M) {
    int a, b;
    cin >> a >> b;
    // Conencts each vertex because G is an undirected graph.
    // If G is an directed graph, each connection is distinct.
    G[a].push_back(b);
    G[b].push_back(a);
  }

  vector<int> dist(N, -1);
  queue<int> q;

  dist[0] = 0; // Distance of the start vertex.
  q.push(0);   // An initial reached vertex.

  while (!q.empty()) {
    int v = q.front();
    q.pop();

    for (int nv : G[v]) {
      if (dist[nv] != -1) {
        // Already reached.
        continue;
      }

      // Set new distance of nv.
      // dist[nv] must be dist[n] + 1
      dist[nv] = dist[v] + 1;
      q.push(nv);
    }
  }

  REP(i, N) { cout << i << ":" << dist[i] << endl; }
}