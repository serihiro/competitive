#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;

  if(N == 1){
    cout <<setprecision(6) <<  1.0 << endl;
    return 0;
  }

  if(N%2 == 0){
    cout << setprecision(6) << 0.500000 << endl;
  }else{
    cout << setprecision(6) << (((N-1) / 2.0) + 1.0) / N << endl;
  }
  
  return 0;
}
