#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;
  vector<int> count(N + 1, 0);
  count.at(1) = 1;
  vector<int> A(N + 1);
  REP(i, N) { cin >> A.at(i + 1); }

  int ans = 0;
  int i = 1;
  while (true) {
    ++ans;
    int next = A.at(i);
    if (next == 2) {
      cout << ans << endl;
      return 0;
    }
    if (count.at(next) > 0) {
      cout << -1 << endl;
      return 0;
    }
    ++count.at(next);
    i = next;
  }

  return 0;
}
