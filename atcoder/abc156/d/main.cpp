#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

const ll MAX = 2000000;
const ll MOD = 1000000007;

vector<ll> fac(MAX, 0ll);
vector<ll> finv(MAX, 0ll);
vector<ll> inv(MAX, 0ll);

void COMinit() {
  fac[0] = fac[1] = 1;
  finv[0] = finv[1] = 1;
  inv[1] = 1;
  for (int i = 2; i <= MAX; i++) {
    fac[i] = fac[i - 1] * i % MOD;
    inv[i] = MOD - inv[MOD % i] * (MOD / i) % MOD;
    finv[i] = finv[i - 1] * inv[i] % MOD;
  }
}

long long COM(int n, int k) {
  if (n < k)
    return 0;
  if (n < 0 || k < 0)
    return 0;
  // n! / (r!(n-r)!)
  return fac.at(n) * (finv.at(k) * finv.at(n - k) % MOD) % MOD;
}
int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);
  COMinit();

  int N, A, B;
  cin >> N >> A >> B;

  ll ans = ll(pow(2, N)) % MOD;

  cout << ans - 1 - COM(N, A) - COM(N, B) << endl;

  return 0;
}
