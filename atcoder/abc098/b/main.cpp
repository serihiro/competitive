#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  string S;
  cin >> N >> S;
  vector<map<char, int>> c_map;
  c_map.push_back({make_pair(S.at(0), 1)});

  REPI(i, 1, N) {
    map<char, int> m = c_map.at(i - 1);
    if (m.find(S.at(i)) == m.end()) {
      m.insert(make_pair(S.at(i), 1));
    } else {
      ++m.at(S.at(i));
    }

    c_map.push_back(m);
  }

  // for (auto s : c_map) {
  //   for (auto c : s) {
  //     cout << c.first << ":" << c.second << endl;
  //   }
  //   cout << endl;
  // }

  int ans = 0;
  REP(i, N) {
    int count = 0;
    auto m1 = c_map.at(i);
    auto m2 = c_map.at(N - 1);
    for (auto const &it : m1) {
      m2.at(it.first) -= it.second;
      if (m2.at(it.first) > 0) {
        ++count;
      }
    }

    ans = max(ans, count);
  }

  cout << ans << endl;

  return 0;
}
