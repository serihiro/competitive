#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  string S;
  cin >> S;
  ll l = S.length();

  ll ans = 2 * (l - 1);

  REPI(i, 2, l) {
    if (S.at(i - 1) == 'U') {
      ans += l - i;
      ans += 2 * (i - 1);
      // cout << i << "/" << l - i << ":" << 2 * (i - 1) << endl;
    } else {
      ans += i - 1;
      ans += 2 * (l - i);
      // cout << i << "/" << i - 1 << ":" << 2 * (l - i) << endl;
    }
  }

  cout << ans << endl;

  return 0;
}
