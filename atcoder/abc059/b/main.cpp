#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  string A, B;
  cin >> A >> B;

  int a_l = A.length();
  int b_l = B.length();
  if (a_l > b_l) {
    cout << "GREATER" << endl;
    return 0;
  }
  if (a_l < b_l) {
    cout << "LESS" << endl;
    return 0;
  }

  REP(i, a_l) {
    if (A.at(i) > B.at(i)) {
      cout << "GREATER" << endl;
      return 0;
    }
    if (A.at(i) < B.at(i)) {
      cout << "LESS" << endl;
      return 0;
    }
  }

  cout << "EQUAL" << endl;

  return 0;
}
