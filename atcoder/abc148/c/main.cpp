#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int gcd(int a, int b) {
  if (b > 0) {
    return gcd(b, a % b);
  } else {
    return a;
  }
}

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  ll A, B;
  cin >> A >> B;

  ll g = gcd(A, B);
  ll ans = A / g * B;

  cout << ans << endl;

  return 0;
}
