def lower_bound(nums, target):
    ok = -1
    ng = len(nums)
    while abs(ok - ng) > 1:
        mid = (ok + ng) // 2
        if nums[mid] <= target:
            ok = mid
        else:
            ng = mid
    return ok


def upper_bound(nums, target):
    ok = len(nums)
    ng = -1
    while abs(ok - ng) > 1:
        mid = (ok + ng) // 2
        if nums[mid] >= target:
            ok = mid
        else:
            ng = mid
    return ok


nums = [2, 4, 6, 8, 10, 12, 14, 16, 18, 20]

print("*** lower bound ***")
for i in range(1, 25):
    res = lower_bound(nums, i)
    if 0 <= res <= 9:
        print(f"{i}, {res}, {nums[res]}")
    else:
        print(f"{i}, {res}")

print("")
print("*** upper bound ***")
for i in range(1, 25):
    res = upper_bound(nums, i)
    if 0 <= res <= 9:
        print(f"{i}, {res}, {nums[res]}")
    else:
        print(f"{i}, {res}")
