#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;
  vector<vector<char>> a(N);
  unordered_map<char, int> count;

  char _a;
  REP(i, N) {
    REP(j, N) {
      cin >> _a;
      a.at(i).push_back(_a);
      if (count.find(_a) == count.end()) {
        count.insert(make_pair(_a, 1));
      } else {
        ++count.at(_a);
      }
    }
  }

  if (N == 1) {
    cout << a.at(0).at(0) << endl;
    return 0;
  }

  int count_d = 0;
  queue<char> q, q2;
  for (auto it : count) {
    if (it.second >= 2) {
      ++count_d;
      q.push(it.first);
    } else {
      q2.push(it.first);
    }
  }

  if (count_d < N / 2) {
    cout << -1 << endl;
    return 0;
  }

  if (N % 2 == 1) {
    if ((int)count.size() < N / 2 + 1) {
      cout << -1 << endl;
      return 0;
    }
  }

  if (N % 2 == 0) {
    string s = "";
    REP(i, N / 2) {
      s += q.front();
      q.pop();
    }
    cout << s;
    reverse(s.begin(), s.end());
    cout << s << endl;
  } else {
    string s = "";
    REP(i, N / 2) {
      s += q.front();
      q.pop();
    }
    cout << s;
    if (!q.empty()) {
      cout << q.front();
    } else {
      cout << q2.front();
    }
    reverse(s.begin(), s.end());
    cout << s << endl;
  }

  return 0;
}
