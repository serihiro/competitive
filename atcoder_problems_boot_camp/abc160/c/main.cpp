#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int K, N;
  cin >> K >> N;
  vector<int> A(N);
  REP(i, N) { cin >> A.at(i); }

  int ans = A.at(N - 1) - A.at(0);
  REPI(i, 1, N) {
    int i_to_start = K - A.at(i);
    int start_to_i = A.at(i - 1);
    ans = min(ans, i_to_start + start_to_i);
  }

  cout << ans << endl;

  return 0;
}
