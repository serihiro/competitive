#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;
  vector<int> count(N + 1, 0);

  int a;
  REPI(i, 1, N + 1) {
    cin >> a;
    ++count.at(a);
  }

  int x = -1;
  int y = -1;
  REPI(i, 1, N + 1) {
    if (count.at(i) == 2) {
      y = i;
      continue;
    }

    if (count.at(i) == 0) {
      x = i;
      continue;
    }
  }

  if (x == -1 && y == -1) {
    cout << "Correct" << endl;
  } else {
    cout << y << " " << x << endl;
  }

  return 0;
}
