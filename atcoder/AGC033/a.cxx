// muripo
// 参考実装: https://atcoder.jp/contests/agc033/submissions/5257462
#include <iostream>
#include <algorithm>
#include <queue>

using namespace std;

typedef long long ll;

int H, W;
static char A[1001][1001];
static bool X[1001][1001];

constexpr int DI[] = {-1, 1, 0, 0};
constexpr int DJ[] = {0, 0, -1, 1};

int main()
{
    cin.tie(0);
    ios::sync_with_stdio(false);

    struct Point
    {
        int i, j, s = 0;
    };
    queue<Point> q;

    cin >> H >> W;
    for (int i = 1; i <= H; ++i)
    {
        for (int j = 1; j <= W; ++j)
        {
            cin >> A[i][j];
            if (A[i][j] == '#')
            {
                X[i][j] = true;
                q.push({i, j, 0});
            }
        }
    }

    int answer, ti, tj;
    // 幅優先探索
    // queueに入ってるのは黒マスのpoint
    // whileループ内ではqueueの各要素に対して前後左右に探索を行い，
    // Aの領域内でかつ白マスを見つけたらqueueuに追加してマークする
    // 白マスが見つけられなくなるまで繰り返す
    // 白マスが見つけられなくなったタイミングでのループ実行回数が解となる
    while (!q.empty())
    {
        auto n = q.front();
        answer = n.s;
        q.pop();

        for (int i = 0; i < 4; ++i)
        {
            ti = n.i + DI[i];
            tj = n.j + DJ[i];
            if (1 <= ti && ti <= H && 1 <= tj && tj <= W && !X[ti][tj])
            {
                X[ti][tj] = true;
                q.push({ti, tj, n.s + 1});
            }
        }
    }

    cout << answer << endl;

    return 0;
}
