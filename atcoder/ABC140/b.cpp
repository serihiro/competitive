
#include <bits/stdc++.h>

#define REP(i, x) for (int i = 1; i <= (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  int A[20];
  int B[20];
  int C[20];

  long result = 0;
  
  cin >> N;
  REP(i, N) {cin >> A[i];}
  REP(i, N) {cin >> B[i];}
  REP(i, N-1) {cin >> C[i];}

  int last_a = -100;
  REP(i, N){
    result += B[A[i]];
    if(A[i] - last_a == 1){
      result += C[last_a];
    }
    last_a = A[i];
  }

  cout << result << endl;

  return 0;
}
