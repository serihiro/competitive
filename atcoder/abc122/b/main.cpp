#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  string S;
  cin >> S;
  int s = -1;
  int l = (int)S.length();

  int ans = 0;
  REP(i, l) {
    if (S.at(i) == 'A' || S.at(i) == 'T' || S.at(i) == 'G' || S.at(i) == 'C') {
      if (s == -1) {
        s = i;
      }
    } else {
      if (s != -1) {
        ans = max(ans, i - s);
        s = -1;
      }
    }
  }

  if (s != -1) {
    ans = max(ans, l - s);
  }

  cout << ans << endl;

  return 0;
}
