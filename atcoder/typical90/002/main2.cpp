#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

bool is_ok(string s) {
  int i = 0;
  for (const char &c : s) {
    if (c == '(') {
      ++i;
    } else {
      --i;
    }
    if (i < 0) {
      return false;
    }
  }

  return i == 0;
}

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;
  if (N % 2 == 1) {
    cout << endl;
    return 0;
  }

  for (int i = 0; i < (1 << N); ++i) {
    string s = "";
    for (int j = N - 1; j >= 0; --j) {
      if ((i & (1 << j)) == 0) {
        s += '(';
      } else {
        s += ')';
      }
    }
    if (is_ok(s)) {
      cout << s << endl;
    }
  }

  return 0;
}
