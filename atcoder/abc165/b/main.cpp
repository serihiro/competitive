#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  ll X;
  cin >> X;

  ll current = 100;
  for (ll i = 0; i < 1e19; ++i) {
    if (current >= X) {
      cout << i << endl;
      return 0;
    }
    current *= 1.01;
    current = floor(current);
  }

  return 0;
}
