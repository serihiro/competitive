#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  string s, p;
  cin >> s >> p;
  s += s.substr(0, p.length());
  REP(i, s.length() - p.length() + 1) {
    bool found = true;
    REP(j, p.length()) {
      if (s.at(i + j) != p.at(j)) {
        found = false;
        break;
      }
    }

    if (found) {
      cout << "Yes" << endl;
      return 0;
    }
  }

  cout << "No" << endl;

  return 0;
}
