#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;

  REPI(i, 1, 50001) {
    int candidate = floor(i * 1.08);
    if (candidate == N) {
      cout << i << endl;
      return 0;
    }
  }

  cout << ":(" << endl;

  return 0;
}
