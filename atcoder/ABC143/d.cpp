#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  int L[2000];

  cin >> N;
  REP(i, N) { cin >> L[i]; }

  ll result = 0;
  sort(L, L + N);

  for (int i = 0; i < N - 2; ++i) {
    for (int j = i + 1; j < N - 1; ++j) {
      for (int k = j + 1; k < N; ++k) {
        if ((L[i] < L[j] + L[k]) && (L[j] < L[k] + L[i])) {
          if (L[k] < L[i] + L[j]) {
            ++result;
          } else {
            break;
          }
        }
      }
    }
  }

  cout << result;

  return 0;
}
