#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int X;
int N, M;
ll ans;

vector<int> C;
vector<vector<int>> A;
vector<int> total;

void dfs(vector<int> b, vector<int> t, ll p) {
  // cout << endl;
  // cout << "---" << endl;
  // for (int bb : b) {
  //   cout << bb << ",";
  // }
  // cout << endl;
  // cout << "p:" << p << endl;
  // cout << "---" << endl;

  if (b.size() > 0) {
    bool f = true;
    REP(i, M) {
      if (t.at(i) < X) {
        f = false;
        break;
      }
    }
    if (f) {
      ans = min(ans, p);
      return;
    }
  }

  if (b.size() == 0) {
    b.push_back(0);
  } else {
    b.push_back(b.back() + 1);
  }

  while (b.size() <= N && b.back() < N) {
    REP(i, M) { t.at(i) += A.at(b.back()).at(i); }
    dfs(b, t, p + C.at(b.back()));
    REP(i, M) { t.at(i) -= A.at(b.back()).at(i); }
    ++b.back();
  }
}

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  ans = 1 << 29;
  cin >> N >> M;
  cin >> X;
  C = vector<int>(N);
  A = vector<vector<int>>(N);
  total = vector<int>(M);

  int a;
  REP(i, N) {
    cin >> C.at(i);
    REP(j, M) {
      cin >> a;
      A.at(i).push_back(a);
      total.at(j) += a;
    }
  }
  REP(i, M) {
    if (total.at(i) < X) {
      cout << -1 << endl;
      return 0;
    }
  }

  dfs({}, vector<int>(M, 0), 0);

  cout << ans << endl;

  return 0;
}
