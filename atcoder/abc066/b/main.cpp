#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  string S;
  cin >> S;

  int l = S.length();

  int ans = 0;
  for (int i = l - 1; i >= 2; --i) {
    if (i % 2 != 0) {
      continue;
    }

    // cout << S.substr(0, i / 2) << ":" << S.substr(i / 2, i / 2) << endl;

    if (S.substr(0, i / 2) == S.substr(i / 2, i / 2)) {
      ans = max(ans, i);
    }
  }

  cout << ans << endl;

  return 0;
}
