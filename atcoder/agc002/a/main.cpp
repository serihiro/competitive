#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  ll a, b;
  cin >> a >> b;

  if (a == 0 || b == 0 || a * b < 0) {
    cout << "Zero" << endl;
    return 0;
  }

  if (a > 0 && b > 0) {
    cout << "Positive" << endl;
    return 0;
  }

  if ((b - a + 1) % 2 == 0) {
    cout << "Positive" << endl;
  } else {
    cout << "Negative" << endl;
  }

  return 0;
}
