#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

vector<string> answer;
int N;

bool is_valid(string s) {
  stack<char> S;
  for (const char &c : s) {
    if (c == '(') {
      S.push('(');
    } else {
      if (S.size() != 0) {
        S.pop();
      } else {
        return false;
      }
    }
  }

  return S.size() == 0;
}

void dfs(string s) {
  if ((int)s.size() == N) {
    answer.push_back(s);
    return;
  }

  dfs(s + ')');
  dfs(s + '(');
}

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  cin >> N;
  if (N % 2 == 1) {
    cout << endl;
    return 0;
  }
  dfs("");
  sort(answer.begin(), answer.end());
  for (const string &s : answer) {
    if (is_valid(s)) {
      cout << s << endl;
    }
  }

  return 0;
}
