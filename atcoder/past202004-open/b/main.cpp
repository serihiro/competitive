#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  string S;
  cin >> S;
  vector<int> c(3);
  REP(i, (int)S.length()) { ++c.at(S.at(i) - 'a'); }
  int max_vote = *max_element(c.begin(), c.end());
  REP(i, 3) {
    if (c.at(i) == max_vote) {
      cout << (char)('a' + i) << endl;
    }
  }

  return 0;
}
