#include <algorithm>
#include <iostream>

using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int A, B;
  cin >> A >> B;

  int ans = 0;
  string i_s;
  for (int i = A; i <= B; ++i) {
    i_s = to_string(i);
    if (i_s[0] == i_s[4] && i_s[1] == i_s[3]) {
      ++ans;
    }
  }

  cout << ans << endl;

  return 0;
}