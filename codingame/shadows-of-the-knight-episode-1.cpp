// https: // www.codingame.com/ide/puzzle/shadows-of-the-knight-episode-1

#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/

int main() {
  int W; // width of the building.
  int H; // height of the building.
  cin >> W >> H;
  cin.ignore();
  int N; // maximum number of turns before game over.
  cin >> N;
  cin.ignore();
  int X;
  int Y;
  cin >> X >> Y;
  cin.ignore();

  int minX = 0;
  int maxX = W - 1;
  int minY = 0;
  int maxY = H - 1;

  // game loop
  while (1) {
    string bombDir; // the direction of the bombs from batman's current location
                    // (U, UR, R, DR, D, DL, L or UL)
    cin >> bombDir;
    cin.ignore();

    cerr << bombDir << "/" << X << ":" << minX << ":" << maxX << "/" << Y
         << endl;

    if (bombDir.find("U") != string::npos) {
      maxY = Y - 1;
    }

    if (bombDir.find("D") != string::npos) {
      minY = Y + 1;
    }

    if (bombDir.find("L") != string::npos) {
      maxX = X - 1;
    }

    if (bombDir.find("R") != string::npos) {
      minX = X + 1;
    }

    X = (minX + maxX + 1) / 2;
    Y = (minY + maxY + 1) / 2;
    cout << X << " " << Y << endl;
  }
}