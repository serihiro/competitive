#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  ll N;
  cin >> N;
  vector<ll> A(1e5 + 1, 0);
  ll a;
  ll sum = 0ll;
  REP(i, N) {
    cin >> a;
    sum += a;
    ++A.at(a);
  }
  ll Q;
  cin >> Q;
  ll b, c;

  REP(i, Q) {
    cin >> b >> c;

    A.at(c) += A.at(b);
    sum += A.at(b) * (c - b);
    A.at(b) = 0;

    cout << sum << endl;
  }

  return 0;
}
