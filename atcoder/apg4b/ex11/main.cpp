#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, n)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N, A, B;
  string op;
  cin >> N >> A;
  for (int i = 1; i <= N; ++i) {
    cin >> op >> B;
    if (op == "+") {
      A += B;
    } else if (op == "-") {
      A -= B;
    } else if (op == "*") {
      A *= B;
    } else {
      if (B != 0) {
        A /= B;
      } else {
        cout << "error" << endl;
        return 0;
      }
    }

    cout << i << ":" << A << endl;
  }

  return 0;
}
