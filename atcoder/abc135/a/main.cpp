#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  ll A, B;
  cin >> A >> B;
  if (A % 2 == B % 2) {
    cout << (A + B) / 2 << endl;
  } else {
    cout << "IMPOSSIBLE" << endl;
  }

  return 0;
}
