// #include <bits/stdc++.h>

// #define REP(i, x) REPI(i, 0, x)
// #define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
// #define ALL(x) (x).begin(), (x).end()

// typedef long long ll;
// using namespace std;

// int main() {
//   cin.tie(0);
//   ios::sync_with_stdio(false);

//   ll X;
//   cin >> X;
//   ll const max = 1000000;
//   for (ll i = 1; i <= max; ++i) {
//     if (100 * i <= X && X <= 105 * i) {
//       cout << 1 << endl;
//       return 0;
//     }
//   }

//   cout << 0 << endl;

//   return 0;
// }

#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  ll X;
  cin >> X;
  vector<bool> dp(101010, false);
  dp.at(0) = true;
  REP(i, 100000) {
    if (dp.at(i)) {
      REP(j, 6) { dp.at(i + 100 + j) = true; }
    }
  }
  if (dp.at(X)) {
    cout << 1 << endl;
  } else {
    cout << 0 << endl;
  }

  return 0;
}
