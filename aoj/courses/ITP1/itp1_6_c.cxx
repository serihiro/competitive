#include <iostream>
#include <vector>
#include <algorithm> // algorithm header is required at AOJ
using namespace std;

#define REP(i, n) for (int i = 0; i < (n); ++i)

int main()
{
    cin.tie(0);
    ios::sync_with_stdio(false);

    int n;
    cin >> n;
    char kind;
    int rank;
    vector<int> cards;

    int c = 0;
    while (c < n && cin >> kind >> rank)
    {
        switch (kind)
        {
        case 'S':
            cards.push_back(rank);
            break;
        case 'H':
            cards.push_back(rank + 13);
            break;
        case 'C':
            cards.push_back(rank + 26);
            break;
        case 'D':
            cards.push_back(rank + 39);
            break;
        }
    }
    sort(cards.begin(), cards.end());

    int current_count = 1;
    int index = 0;
    vector<int> missing_cards;

    while (current_count < 53)
    {
        if (index > cards.size() - 1)
        {
            missing_cards.push_back(current_count);
            current_count += 1;
            continue;
        }

        if (cards[index] != current_count)
        {
            missing_cards.push_back(current_count);
        }
        else
        {
            ++index;
        }
        ++current_count;
    }

    // printf("nya-n: %lu\n", missing_cards.size());

    if (missing_cards.size() == 0)
    {
        return 0;
    }
    else
    {
        int rank;
        int adjust = 0;
        REP(i, missing_cards.size())
        {
            if (missing_cards[i] % 13 == 0)
            {
                rank = 13;
                adjust = -1;
            }
            else
            {
                rank = missing_cards[i] % 13;
                adjust = 0;
            }

            switch (missing_cards[i] / 13 + adjust)
            {
            case 0:
                cout << "S " << rank << endl;
                break;
            case 1:
                cout << "H " << rank << endl;
                break;
            case 2:
                cout << "C " << rank << endl;
                break;
            case 3:
                cout << "D " << rank << endl;
                break;
            case 4:
                cout << "D " << rank << endl;
            }
        }
    }

    return 0;
}