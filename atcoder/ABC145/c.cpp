#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  double X[8];
  double Y[8];
  cin >> N;
  REP(i, N) {
    cin >> X[i];
    cin >> Y[i];
  }

  double sum = 0.0;
  for (int i = 0; i < N; ++i) {
    for (int j = i + 1; j < N; ++j) {
      sum +=
          sqrt((X[i] - X[j]) * (X[i] - X[j]) + (Y[i] - Y[j]) * (Y[i] - Y[j]));
    }
  }
  cout << setprecision(20) << sum * 2 / N << endl;

  return 0;
}
