#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int keta(ll n) {
  if (n == 0) {
    return 1;
  }
  int ans = 0;
  while (n > 0) {
    n /= 10;
    ++ans;
  }

  return ans;
}

void dfs(ll current, vector<ll> &list, int K) {
  if (static_cast<int>(list.size()) >= K)
    return;

  ll digit = current % 10;
  if (digit == 0) {
    list.push_back(current + digit);
    list.push_back(current + digit + 1);
  } else if (digit <= 9) {
    list.push_back(current + digit - 1);
    list.push_back(current + digit);
    list.push_back(current + digit + 1);
  } else {
    list.push_back(current + digit - 1);
    list.push_back(current + digit);
  }

  dfs(current + ), list, K);
}

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int K;

  cin >> K;
  if (K < 10) {
    cout << K << endl;
    return 0;
  }

  vector<ll> list = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

  dfs(10, list, K);
  sort(list.begin(), list.end());

  for (ll i : list) {
    cout << i << endl;
  }

  cout << list.at(K - 1) << endl;

  return 0;
}
