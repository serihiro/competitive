#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N, tmp_a;
  int A[1000001];

  cin >> N;
  REP(i,N){
    cin >> tmp_a;
    A[tmp_a - 1] = i;
  }

  bool first = true;
  REP(i, N){
    if(first){
      cout << A[i] + 1;
      first = false;
    }else{
      cout << " " << A[i] + 1;
    }
  }
  cout << endl;

  return 0;
}
