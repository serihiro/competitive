// https://beta.atcoder.jp/contests/abc092/tasks/arc093_a
// giveup

#include <iostream>
#include <cstdlib>
using namespace std;

int main()
{
    cin.tie(0);
    ios::sync_with_stdio(false);

    int N;
    long long sum = 0;
    int A[100002] = {};

    cin >> N;
    for(int i=1; i<=N; ++i){
        cin >> A[i];
        sum += abs(A[i] - A[i-1]);
    }
    sum += abs(A[N+1] - A[N]);

    for(int j=1; j<=N; ++j){
        cout << sum - abs(A[j] - A[j-1]) - abs(A[j+1] - A[j]) + abs(A[j+1] - A[j-1]) << endl;
    }

    return 0;
}
