#include <iostream>
using namespace std;

int main()
{
    cin.tie(0);
    ios::sync_with_stdio(false);

    int A, B, C, D, train, bas;
    cin >> A >> B >> C >> D;

    if (A > B)
    {
        train = B;
    }
    else
    {
        train = A;
    }

    if (C > D)
    {
        bas = D;
    }
    else
    {
        bas = C;
    }

    cout << bas + train << endl;

    return 0;
}
