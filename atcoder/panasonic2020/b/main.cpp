// #include <bits/stdc++.h>

// #define REP(i, x) REPI(i, 0, x)
// #define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
// #define ALL(x) (x).begin(), (x).end()

// typedef long long ll;
// using namespace std;

// int main() {
//   cin.tie(0);
//   ios::sync_with_stdio(false);

//   int H, W;
//   cin >> H >> W;

//   vector<vector<bool>> visit(H + 1);
//   REP(i, H + 1) {
//     REP(j, W + 1) { visit.at(i).push_back(false); }
//   }

//   visit.at(1).at(1) = true;
//   ll ans = 1ll;

//   queue<pair<int, int>> q;
//   q.push(make_pair(1, 1));

//   vector<int> x = {-1, 1};
//   vector<int> y = {-1, 1};

//   while (!q.empty()) {
//     auto v = q.front();
//     q.pop();

//     REP(i, 2) {
//       REP(j, 2) {
//         if (1 <= v.first + x.at(i) && v.first + x.at(i) <= W &&
//             1 <= v.second + y.at(j) && v.second + y.at(j) <= H &&
//             ((v.first + v.second == v.first + x.at(i) + v.second + y.at(j))
//             ||
//              (v.first - v.second ==
//               v.first + x.at(i) - (v.second + y.at(j))))) {
//           if (!visit.at(v.second + y.at(j)).at(v.first + x.at(i))) {
//             ++ans;
//             visit.at(v.second + y.at(j)).at(v.first + x.at(i)) = true;
//             q.push(make_pair(v.first + x.at(i), v.second + y.at(j)));
//           }
//         }
//       }
//     }
//   }

//   cout << ans << endl;

//   return 0;
// }

#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  ll H, W;
  cin >> H >> W;

  ll ans = 0ll;
  if (H == 1 || W == 1) {
    cout << 1 << endl;
    return 0;
  }

  if (H % 2 == 0) {
    if (W % 2 == 0) {
      ans = H * W / 2;
    } else {
      ans = H * (W - 1) / 2 + ceil(H / 2.0);
    }
  } else {
    ans = (H - 1) * (W - 1) / 2 + ceil(H / 2.0) + ceil(W / 2.0) - 1;
  }

  cout << ans << endl;

  return 0;
}