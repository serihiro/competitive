/*
9 13
0 1
0 4
0 2
1 4
1 3
1 8
2 5
3 8
4 8
5 8
5 6
3 7
6 7
*/

#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;
using Graph = vector<vector<int>>; // G[E][V]

int main() {
  int N; // N is the size of the vertexes.
  int M; // M is the size of the edges.

  cin >> N >> M;
  Graph G(N);

  int a, b;

  REP(i, M) {
    cin >> a >> b;
    G.at(a).push_back(b);
    G.at(b).push_back(a);
  }

  vector<int> dist(N, -1);
  queue<int> Q;
  Q.push(0);
  dist.at(0) = 0;
  while (!Q.empty()) {
    int v = Q.front();
    Q.pop();

    for (int nv : G.at(v)) {
      if (dist.at(nv) != -1) {
        continue;
      }

      dist.at(nv) = dist.at(v) + 1;
      Q.push(nv);
    }
  }

  REP(i, N) { cout << i << ":" << dist[i] << endl; }

  return 0;
}
