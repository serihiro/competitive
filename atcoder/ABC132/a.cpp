#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  char a, b, c, d;
  cin >> a >> b >> c >> d;

  if (a == b && c == d && a != c && a != d) {
    cout << "YES" << endl;
  } else if (a == c && b == d && a != b && a != d) {
    cout << "YES" << endl;
  } else if (a == d && b == c && a != b && a != c) {
    cout << "YES" << endl;
  } else {
    cout << "NO" << endl;
  }

  return 0;
}
