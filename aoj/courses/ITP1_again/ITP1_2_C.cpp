#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  vector<int> A(3);
  REP(i, 3) { cin >> A.at(i); }
  sort(A.begin(), A.end());
  cout << A.at(0) << " " << A.at(1) << " " << A.at(2) << endl;

  return 0;
}
