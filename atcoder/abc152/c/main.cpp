#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;
  vector<int> P(N);
  REP(i, N) { cin >> P[i]; }
  int min_p = P[0];
  int ans = 1;
  REPI(i, 1, N) {
    min_p = min(min_p, P[i]);
    if (min_p == P[i]) {
      ++ans;
    }
  }

  cout << ans << endl;

  return 0;
}
