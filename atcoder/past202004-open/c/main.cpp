#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;
  vector<string> S(N, "");

  char c;
  REP(i, N) {
    REP(j, 2 * N - 1) {
      cin >> c;
      S.at(i) += c;
    }
  }

  for (int i = N - 2; i >= 0; --i) {
    for (int j = 1; j < 2 * N - 1; ++j) {
      // cout << i << ":" << j << endl;

      if (S.at(i).at(j) == '#') {
        if (S.at(i + 1).at(j - 1) == 'X' || S.at(i + 1).at(j) == 'X' ||
            S.at(i + 1).at(j + 1) == 'X') {
          S.at(i).at(j) = 'X';
        }
      }
    }
  }

  REP(i, N) {
    REP(j, 2 * N - 1) { cout << S.at(i).at(j); }
    cout << endl;
  }

  return 0;
}
