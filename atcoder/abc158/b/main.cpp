#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  ll N, A, B;
  cin >> N >> A >> B;

  ll kaisuu = N / (A + B);
  ll amari = N % (A + B);
  if (kaisuu == 0) {
    cout << min(A, N) << endl;
  } else {
    cout << kaisuu * A + min(amari, A);
  }

  return 0;
}
