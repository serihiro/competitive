#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  ll N, K;
  cin >> N >> K;

  if (N == 1) {
    cout << K << endl;
    return 0;
  }

  cout << setprecision(30) << K * pow(K - 1, N - 1) << endl;

  return 0;
}
