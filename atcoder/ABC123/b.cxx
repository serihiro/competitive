#include <iostream>
#include <algorithm>

using namespace std;

int main()
{
    cin.tie(0);
    ios::sync_with_stdio(false);

    int A[5];
    cin >> A[0];
    cin >> A[1];
    cin >> A[2];
    cin >> A[3];
    cin >> A[4];

    int a[5];
    a[0] = A[0] % 10;
    a[1] = A[1] % 10;
    a[2] = A[2] % 10;
    a[3] = A[3] % 10;
    a[4] = A[4] % 10;

    int min_i = -1;
    for (int i = 0; i < 5; ++i)
    {
        if (min_i == -1)
        {
            min_i = i;
        }

        if (a[i] != 0 && a[min_i] > a[i])
        {
            min_i = i;
        }
    }

    int result = 0;
    for (int i = 0; i < 5; ++i)
    {
        if (i != min_i)
        {
            if (a[i] != 0)
            {
                result = result + A[i] + 10 - a[i];
            }
            else
            {
                result = result + A[i];
            }
        }
    }
    result += A[min_i];

    cout << result << endl;

    return 0;
}
