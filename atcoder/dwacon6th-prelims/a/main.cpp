#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;
  string s[50];
  string x;
  int sum_t = 0;
  int t[50];
  REP(i, N) {
    cin >> s[i];
    cin >> t[i];
    sum_t += t[i];
  }
  cin >> x;
  int current_sum = 0;
  int ans = 0;
  REP(i, N) {
    if (s[i] == x) {
      ans = sum_t - current_sum - t[i];
      break;
    }
    current_sum += t[i];
  }

  cout << ans << endl;

  return 0;
}
