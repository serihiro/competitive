// wakaran
#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  ll N;
  cin >> N;

  ll result = 1e13;
  ll j = 0;
  for (int i = 1; i <= sqrt(N); ++i) {
    j = N / i;

    if (i * j == N) {
      result = min(result, static_cast<ll>(i + j - 2));
    }
  }

  cout << result << endl;

  return 0;
}
