// https://beta.atcoder.jp/contests/abc091
#include <iostream>
#include <map>
using namespace std;

int main()
{
    cin.tie(0);
    ios::sync_with_stdio(false);

    int N, M;
    map<string, int> s = {};
    string tmp;

    cin >> N;

    for (int i = 0; i < N; ++i)
    {
        cin >> tmp;
        if (s.find(tmp) == s.end())
        {
            s[tmp] = 0;
        }
        ++s[tmp];
    }

    cin >> M;

    for (int i = 0; i < M; ++i)
    {
        cin >> tmp;
        if (s.find(tmp) != s.end())
        {
            --s[tmp];
        }
    }

    int ans = 0;
    for (auto itr = s.begin(); itr != s.end(); ++itr)
    {
        if (ans < itr->second)
        {
            ans = itr->second;
        }
    }

    cout << ans << endl;
    return 0;
}
