#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);
  
  ll N, Q, K;

  cin >> N >> K >> Q;

  ll score[100001];
  REP(i, N+1) {score[i] = 0;}

  int tmp_a;
  REP(i, Q) {
    cin >> tmp_a;
    ++score[tmp_a];
  }

  for(long i=1; i<=N; ++i){
    if(score[i] + K - Q > 0){
      cout << "Yes" << endl;
    }else{
      cout << "No" << endl;
    }
  }

  return 0;
}
