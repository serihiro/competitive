#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  ll W, H, x, y;
  cin >> W >> H >> x >> y;

  bool f = 2 * x == W && 2 * y == H;
  cout << setprecision(20) << (double)W * (double)H / 2.0 << " " << (int)f
       << endl;

  return 0;
}
