#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  ll result = 9999999999999;
  int D;
  ll a[200000];
  ll b[200000];

  cin >> D;
  REP(i, D) { cin >> a[i]; }
  REP(i, D) { cin >> b[i]; }

  ll kozukai = 0;
  REP(i, D) {
    kozukai += a[i];
    if (b[i] <= kozukai) {
      result = min(b[i], result);
    }
  }

  if (result == 9999999999999) {
    cout << -1 << endl;
  } else {
    cout << result << endl;
  }

  return 0;
}
