#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  ll A, B, C, K;
  cin >> A >> B >> C >> K;

  ll ans = 0;
  ans += min(A, K);
  K -= A;
  if (K <= 0) {
    cout << ans << endl;
    return 0;
  }

  K -= B;
  if (K <= 0) {
    cout << ans << endl;
    return 0;
  }

  ans -= min(K, C);
  cout << ans << endl;

  return 0;
}
