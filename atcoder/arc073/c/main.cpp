#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  ll N, T;
  cin >> N >> T;
  vector<ll> diff(N - 1);
  ll t, prev_t;
  cin >> prev_t;

  REP(i, N - 1) {
    cin >> t;
    diff.at(i) = t - prev_t;
    prev_t = t;
  }

  ll ans = 0;
  REP(i, N - 1) { ans += min(T, diff.at(i)); }
  ans += T;

  cout << ans << endl;

  return 0;
}
