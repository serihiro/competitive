#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;
  map<string, int> count;
  string s;
  int max_count = 0;
  REP(i, N) {
    cin >> s;
    if (count.find(s) != count.end()) {
      ++count.at(s);
      max_count = max(max_count, count.at(s));
    } else {
      count.insert(make_pair(s, 0));
    }
  }

  for (auto it : count) {
    if (it.second == max_count) {
      cout << it.first << endl;
    }
  }
  return 0;
}