#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  string S;
  cin >> S;
  int l = S.length();
  vector<int> b(l + 1, 0);

  for (int i = 0; i < l; ++i) {
    if (S.at(i) == '<') {
      b.at(i + 1) = b.at(i) + 1;
    }
  }

  for (int i = l - 1; i >= 0; --i) {
    if (S.at(i) == '>') {
      b.at(i) = max(b.at(i), b.at(i + 1) + 1);
    }
  }

  // REP(i, l + 1) { cout << b.at(i) << ","; }
  // cout << endl;

  // REP(i, l + 1) { cout << b.at(i) << ","; }
  // cout << endl;

  ll ans = accumulate(b.begin(), b.end(), 0ll);
  cout << ans << endl;

  return 0;
}
