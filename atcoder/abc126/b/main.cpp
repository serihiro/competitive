#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  string S;
  cin >> S;
  bool yymm1 = false;
  bool yymm2 = false;
  bool mmyy1 = false;
  bool mmyy2 = false;

  int a = stoi(S.substr(0, 2));
  if (1 <= a && a <= 12) {
    mmyy1 = true;
    yymm1 = true;
  } else {
    yymm1 = true;
  }

  int b = stoi(S.substr(2, 2));
  if (1 <= b && b <= 12) {
    yymm2 = true;
    mmyy2 = true;
  } else {
    mmyy2 = true;
  }

  if (yymm1 && yymm2) {
    if (mmyy1 && mmyy2) {
      cout << "AMBIGUOUS" << endl;
    } else {
      cout << "YYMM" << endl;
    }
  } else if (mmyy1 && mmyy2) {
    cout << "MMYY" << endl;
  } else {
    cout << "NA" << endl;
  }

  return 0;
}
