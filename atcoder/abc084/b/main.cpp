#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int A, B;
  cin >> A >> B;
  string S;
  cin >> S;

  REP(i, A) {
    if (!('0' <= S.at(i) && S.at(i) <= '9')) {
      cout << "No" << endl;
      return 0;
    }
  }
  if (S.at(A) != '-') {
    cout << "No" << endl;
    return 0;
  }

  REP(i, B) {
    if (!('0' <= S.at(A + 1 + i) && S.at(A + 1 + i) <= '9')) {
      cout << "No" << endl;
      return 0;
    }
  }

  cout << "Yes" << endl;

  return 0;
}
