#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  string x;
  while (cin >> x) {
    if (x == "0") {
      return 0;
    }
    int ans = 0;
    REP(i, (int)x.length()) { ans += x.at(i) - '0'; }
    cout << ans << endl;
  }

  return 0;
}
