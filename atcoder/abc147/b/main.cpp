#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  string S;
  cin >> S;

  int ans = 0;
  int l = S.length();
  REP(i, l / 2) {
    if (S[i] != S[l - 1 - i]) {
      ++ans;
    }
  }

  cout << ans << endl;

  return 0;
}
