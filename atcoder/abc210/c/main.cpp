#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N, K;
  cin >> N >> K;
  vector<ll> C(N, 0ll);
  map<ll, int> S;
  int ans = 1;

  REP(i, N) {
    cin >> C.at(i);
    if (S.find(C.at(i)) == S.end()) {
      S.insert(make_pair(C.at(i), 1));
    } else {
      ++S.at(C.at(i));
    }


    if (i >= K) {
      --S.at(C.at(i - K));
      if (S.at(C.at(i - K)) == 0) {
        S.erase(C.at(i - K));
      }
    }

    ans = max(ans, (int)S.size());
  }

  cout << ans << endl;

  return 0;
}
