#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  string S;
  cin >> S;
  int l = (int)S.length();
  set<string> ans;

  REPI(i, 1, 4) {
    for (int j = 0; j <= l - i; ++j) {
      ans.insert(S.substr(j, i));
      if (i == 1) {
        ans.insert(".");
      } else if (i == 2) {
        ans.insert("..");
        string _s = "";
        _s += S.at(j);
        _s += '.';
        ans.insert(_s);
        _s = '.';
        _s += S.at(j + 1);
        ans.insert(_s);
      } else {
        ans.insert("...");

        string _s = ".";
        _s += S.at(j + 1);
        _s += S.at(j + 2);
        ans.insert(_s);

        _s = S.at(j);
        _s += '.';
        _s += S.at(j + 2);
        ans.insert(_s);

        _s = S.substr(j, 2);
        _s += '.';
        ans.insert(_s);

        _s = "..";
        _s += S.at(j + 2);
        ans.insert(_s);

        _s = S.at(j);
        _s += "..";
        ans.insert(_s);

        _s = ".";
        _s += S.at(j + 1);
        _s += ".";
        ans.insert(_s);
      }
    }
  }

  cout << ans.size() << endl;

  return 0;
}
