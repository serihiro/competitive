#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;

  vector<int> a(N);
  REP(i, N) { cin >> a.at(i); }

  int cur = 1;
  REP(i, N) {
    if (a[i] == cur) {
      cur++;
    }
  }

  int ans = N - (cur - 1);
  if (cur == 1) {
    cout << -1 << endl;
  } else {
    cout << ans << endl;
  }

  return 0;
}
