A = []
for _ in range(3):
    A.append(input().split())
N = int(input())
B = []
for _ in range(N):
    B.append(input())

for i in range(3):
    if (A[i][0] in B) and (A[i][1] in B) and (A[i][2] in B):
        print("Yes")
        exit(0)

    if (A[0][i] in B) and (A[1][i] in B) and (A[2][i] in B):
        print("Yes")
        exit(0)

if (A[0][0] in B) and (A[1][1] in B) and (A[2][2] in B):
    print("Yes")
    exit(0)

if (A[0][2] in B) and (A[1][1] in B) and (A[2][0] in B):
    print("Yes")
    exit(0)

print("No")
