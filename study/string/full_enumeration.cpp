#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

void f(int n, string s, char c) {
  if (n == 1) {
    s += c;
    cout << s << endl;
    return;
  }

  if (c != '\0') {
    s += c;
    --n;
  }

  f(n, s, 'A');
  f(n, s, 'T');
  f(n, s, 'C');
  f(n, s, 'G');
}

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;

  f(N, "", '\0');

  return 0;
}
