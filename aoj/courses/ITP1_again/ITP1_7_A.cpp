#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int m, f, r;

  while (cin >> m >> f >> r) {
    if (m == -1 && f == -1 && r == -1) {
      return 0;
    }

    if (m == -1 || f == -1) {
      cout << 'F' << endl;
      continue;
    }

    if (m + f >= 80) {
      cout << 'A' << endl;
      continue;
    }

    if ((65 <= (m + f)) && ((m + f) < 80)) {
      cout << 'B' << endl;
      continue;
    }

    if ((50 <= (m + f)) && ((m + f) < 65)) {
      cout << 'C' << endl;
      continue;
    }

    if ((30 <= (m + f)) && ((m + f) < 50)) {
      if (r >= 50) {
        cout << 'C' << endl;
      } else {
        cout << 'D' << endl;
      }
      continue;
    }

    cout << 'F' << endl;
  }

  return 0;
}
