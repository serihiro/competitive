#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int r, c;
  cin >> r >> c;
  vector<int> sr(r, 0);
  vector<int> cr(c, 0);

  vector<vector<int>> t(r, vector<int>(c));

  int v;
  int total = 0;
  REP(i, r) {
    REP(j, c) {
      cin >> v;
      t.at(i).at(j) = v;
      sr.at(i) += v;
      cr.at(j) += v;
      total += v;
    }
  }

  REP(i, r) {
    REP(j, c) { cout << t.at(i).at(j) << " "; }
    cout << sr.at(i) << endl;
  }
  REP(j, c) { cout << cr.at(j) << " "; }
  cout << total << endl;

  return 0;
}
