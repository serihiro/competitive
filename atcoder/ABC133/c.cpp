#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  ll L, R;
  ll ans = 1 << 30;
  ll i, j;
  cin >> L >> R;

  for (i = L; i <= L + 2019 && i <= R; ++i) {
    for (j = i + 1; j <= j + 2018 && j <= R; ++j) {
      if (((i * j) % 2019) < ans) {
        // cout << i << "," << j << "," << (i * j) % 2019 << endl;
        ans = (i * j) % 2019;
      }
      if (ans == 0) {
        cout << ans << endl;
        return 0;
      }
    }
  }

  cout << ans << endl;

  return 0;
}