#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N, M;
  int k;
  cin >> N >> M;
  vector<int> count(M, 0);

  int a;
  REP(i, N) {
    cin >> k;
    REP(j, k) {
      cin >> a;
      --a;
      ++count.at(a);
    }
  }

  int ans = 0;
  REP(i, M) {
    if (count.at(i) == N) {
      ++ans;
    }
  }

  cout << ans << endl;

  return 0;
}
