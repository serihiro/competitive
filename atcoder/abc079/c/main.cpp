#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  string s;
  cin >> s;
  int a = s.at(0) - '0';
  int b = s.at(1) - '0';
  int c = s.at(2) - '0';
  int d = s.at(3) - '0';
  for (char const &c1 : "+-") {
    for (char const &c2 : "+-") {
      for (char const &c3 : "+-") {
        int tmp = 0;
        if (c1 == '+') {
          tmp = a + b;
        } else {
          tmp = a - b;
        }

        if (c2 == '+') {
          tmp = tmp + c;
        } else {
          tmp = tmp - c;
        }

        if (c3 == '+') {
          tmp = tmp + d;
        } else {
          tmp = tmp - d;
        }

        if (tmp == 7) {
          cout << a << c1 << b << c2 << c << c3 << d << "=7" << endl;
          return 0;
        }
      }
    }
  }

  return 0;
}
