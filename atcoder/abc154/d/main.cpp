#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  vector<double> pp(1001);
  for (int i = 1; i < 1001; ++i) {
    pp.at(i) = (((double(i) * (double(i) + 1.0)) / 2.0)) / double(i);
  }

  int N, K;
  cin >> N >> K;
  vector<double> p(N);
  int t_p;
  cin >> t_p;
  p.at(0) = pp.at(t_p);
  REPI(i, 1, N) {
    cin >> t_p;
    p.at(i) = p.at(i - 1) + pp.at(t_p);
  }

  // for (double d : p) {
  //   cout << d << ",";
  // }
  // cout << endl;

  double ans = p.at(K - 1);
  REPI(i, K, N) { ans = max(ans, p.at(i) - p.at(i - K)); }

  cout << setprecision(10) << ans << endl;

  return 0;
}
