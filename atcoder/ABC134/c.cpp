#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int A[200000];
int B[200000];

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;
  REP(i, N) cin >> A[i];
  memcpy(B, A, sizeof(A));
  sort(B, B + N);
  int most_biggest = B[N - 1];
  int second_biggest = B[N - 2];

  REP(i, N) {
    A[i] != most_biggest ? cout << most_biggest << endl
                         : cout << second_biggest << endl;
  }

  return 0;
}
