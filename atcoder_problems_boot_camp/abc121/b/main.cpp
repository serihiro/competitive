#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N, M, C;
  cin >> N >> M >> C;
  vector<int> B(M);

  REP(i, M) { cin >> B.at(i); }

  int a;
  int ans = 0;
  REP(i, N) {
    int sum = 0;
    REP(j, M) {
      cin >> a;
      sum += a * B.at(j);
    }
    if (sum + C > 0) {
      ++ans;
    }
  }

  cout << ans << endl;

  return 0;
}
