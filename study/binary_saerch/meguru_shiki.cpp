#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int upper_bound(vector<int> &a, int v) {
  int ok = a.size();
  int ng = -1;
  while (abs(ok - ng) > 1) {
    int mid = (ok + ng) / 2;
    if (a.at(mid) >= v) {
      ok = mid;
    } else {
      ng = mid;
    }
  }
  return ok;
}

int lower_bound(vector<int> &a, int v) {
  int ok = -1;
  int ng = a.size();
  while (abs(ok - ng) > 1) {
    int mid = (ok + ng) / 2;
    if (a.at(mid) <= v) {
      ok = mid;
    } else {
      ng = mid;
    }
  }
  return ok;
}

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  vector<int> A = {2, 4, 6, 8, 10, 12, 14, 16, 18, 20};
  cout << "*** lower_bound ***" << endl;
  REPI(i, 1, 25) {
    int ans = lower_bound(A, i);
    cout << i << ":" << ans;
    if (0 <= ans && ans <= ((int)A.size() - 1)) {
      cout << ":" << A.at(ans);
    }
    cout << endl;
  }

  cout << endl;

  cout << "*** upper bound ***" << endl;
  REPI(i, 1, 25) {
    int ans = upper_bound(A, i);
    cout << i << ":" << ans;
    if (0 <= ans && ans <= ((int)A.size() - 1)) {
      cout << ":" << A.at(ans);
    }
    cout << endl;
  }

  return 0;
}
