#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;
  if (N == 1) {
    cout << 1 << endl;
    return 0;
  }

  vector<vector<int>> memo(10, vector<int>(10, 0));
  string tmp_n;
  int tmp_i, tmp_j;
  REPI(i, 1, N + 1) {
    tmp_n = to_string(i);
    tmp_i = tmp_n.front() - '0';
    tmp_j = tmp_n.back() - '0';
    ++memo.at(tmp_i).at(tmp_j);
  }

  int ans = 0;

  REPI(i, 1, 10) {
    REPI(j, 1, 10) { ans += memo.at(i).at(j) * memo.at(j).at(i); }
  }

  cout << ans << endl;

  return 0;
}
