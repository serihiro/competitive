#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N, K;
  cin >> N >> K;
  unordered_map<int, int> a1;
  map<int, int> a2;
  int a;
  REP(i, N) {
    cin >> a;
    if (a1.find(a) == a1.end()) {
      a1.insert(make_pair(a, 1));
    } else {
      ++a1.at(a);
    }
  }

  if ((int)a1.size() <= K) {
    cout << 0 << endl;
    return 0;
  }

  int diff = a1.size() - K;

  for (auto const &it : a1) {
    if (a2.find(it.second) == a2.end()) {
      a2.insert(make_pair(it.second, 1));
    } else {
      ++a2.at(it.second);
    }
  }

  int ans = 0;
  for (auto const &it : a2) {
    // cout << it.first << ":" << it.second << ":" << diff << endl;

    if (it.second < diff) {
      ans += it.first * it.second;
      diff -= it.second;
    } else {
      ans += it.first * diff;
      break;
    }

    if (diff == 0) {
      break;
    }
  }

  cout << ans << endl;

  return 0;
}
