#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int H, W;
  cin >> H >> W;

  REP(i, W + 2) { cout << '#'; }
  cout << endl;
  char a;
  REP(i, H) {
    cout << '#';
    REP(j, W) {
      cin >> a;
      cout << a;
    }
    cout << '#';
    cout << endl;
  }
  REP(i, W + 2) { cout << '#'; }
  cout << endl;

  return 0;
}
