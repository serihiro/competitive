#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;
  vector<int> A(N);
  REP(i, N) {
    cin >> A.at(i);
    --A.at(i);
  }

  REP(i, N) {
    int ans = 1;
    int j = i;
    while (i != A.at(j)) {
      j = A.at(j);
      ++ans;
    }
    cout << ans << " ";
  }
  cout << endl;

  return 0;
}
