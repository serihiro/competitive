#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int L;
  cin >> L;

  double ans = 0.0;
  double edge = L / 3.0;
  ans = edge * edge * edge;

  cout << fixed << setprecision(32) << ans << endl;

  return 0;
}
