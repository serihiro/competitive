#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

char down_case(char c) {
  if ('a' <= c && c <= 'z') {
    return c;
  } else {
    return char('a' + (c - 'A'));
  }
}

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  string W, T;
  cin >> W;
  int ans = 0;
  while (cin >> T) {
    if (T == "END_OF_TEXT") {
      break;
    }

    if (W.length() != T.length()) {
      continue;
    }

    bool found = true;
    REP(i, W.length()) {
      if (W.at(i) != down_case(T.at(i))) {
        found = false;
        break;
      }
    }
    if (found) {
      ++ans;
    }
  }

  cout << ans << endl;

  return 0;
}
