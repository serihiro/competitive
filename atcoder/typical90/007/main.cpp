#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

bool is_ok(int mid, vector<ll> &v, ll &b) { return v.at(mid) >= b; }

int binary_search(vector<ll> &v, ll &b) {
  int ng = -1;
  int ok = v.size();
  while (abs(ok - ng) > 1) {
    int mid = (ok + ng) / 2;
    if (is_ok(mid, v, b)) {
      ok = mid;
    } else {
      ng = mid;
    }
  }

  return ok;
}

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;
  vector<ll> A(N);
  REP(i, N) { cin >> A.at(i); }
  sort(A.begin(), A.end());
  int Q;
  cin >> Q;
  REP(i, Q) {
    ll b;
    cin >> b;
    ll upper = binary_search(A, b);
    ll ans1 = 2000000000;
    ll ans2 = 2000000000;
    if (upper <= N - 1) {
      ans1 = abs(b - A.at(upper));
    }
    if (upper >= 1) {
      ans2 = abs(b - A.at(upper - 1));
    }
    cout << min(ans1, ans2) << endl;
  }

  return 0;
}
