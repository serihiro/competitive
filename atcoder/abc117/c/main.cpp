#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N, M;
  cin >> N >> M;
  vector<int> L(M);
  REP(i, M) { cin >> L.at(i); }
  sort(L.begin(), L.end());
  vector<int> L2(M - 1);
  REP(i, M - 1) { L2.at(i) = L.at(i + 1) - L.at(i); }
  sort(L2.begin(), L2.end());

  int ans = 0;
  REP(i, M - 1 - (N - 1)) { ans += L2.at(i); }

  cout << ans << endl;

  return 0;
}
