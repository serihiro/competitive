#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  string S;
  cin >> N >> S;

  ll r = 0;
  ll g = 0;
  ll b = 0;
  REP(i, N) {
    if (S.at(i) == 'R') {
      ++r;
    } else if (S.at(i) == 'G') {
      ++g;
    } else {
      ++b;
    }
  }

  ll ans = r * g * b;

  REP(i, N) {
    REPI(j, i + 1, N) {
      int k = j + (j - i);
      if (k >= N)
        break;

      if (S.at(i) != S.at(j) && S.at(i) != S.at(k) && S.at(j) != S.at(k)) {
        --ans;
      }
    }
  }

  cout << ans << endl;

  return 0;
}
