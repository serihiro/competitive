#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N, K;
  cin >> N >> K;
  // for (int i = 0;; i++) {
  //   if (i * (K - 1) + 1 >= N) {
  //     cout << i << endl;
  //     return 0;
  //   }
  // }
  // m >= (N-1)/(K-1)
  cout << (N + K - 3) / (K - 1) << endl;

  return 0;
}
