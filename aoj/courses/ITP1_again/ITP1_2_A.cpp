#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int A, B;
  cin >> A >> B;
  if (A < B) {
    cout << "a < b" << endl;
  } else if (A > B) {
    cout << "a > b" << endl;
  } else {
    cout << "a == b" << endl;
  }

  return 0;
}
