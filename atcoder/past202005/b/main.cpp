#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N, M, Q;
  cin >> N >> M >> Q;
  map<int, int> score;
  REP(i, M) { score.insert(make_pair(i, N)); }
  vector<vector<int>> solved(N);

  int q, n, m;
  REP(i, Q) {
    cin >> q;
    if (q == 1) {
      cin >> n;
      --n;
      int s = 0;
      for (int j : solved.at(n)) {
        s += score.at(j);
      }
      cout << s << endl;
    } else {
      cin >> n >> m;
      --n;
      --m;
      --score.at(m);
      solved.at(n).push_back(m);
    }
  }

  return 0;
}
