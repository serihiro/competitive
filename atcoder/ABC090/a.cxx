#include <algorithm>
#include <iostream>

using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  string s[3];
  for (int i = 0; i < 3; ++i)
    cin >> s[i];

  cout << s[0][0] << s[1][1] << s[2][2] << endl;

  return 0;
}
