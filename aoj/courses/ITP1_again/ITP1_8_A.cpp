#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  string S;
  getline(cin, S);
  REP(i, (int)S.length()) {
    if ('a' <= S.at(i) && S.at(i) <= 'z') {
      cout << char('A' + (S.at(i) - 'a'));
    } else if ('A' <= S.at(i) && S.at(i) <= 'Z') {
      cout << char('a' + (S.at(i) - 'A'));
    } else {
      cout << S.at(i);
    }
  }
  cout << endl;

  return 0;
}
