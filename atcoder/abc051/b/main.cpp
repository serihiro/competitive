#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int K, S, z;
  int ans = 0;
  cin >> K >> S;

  for (int x = 0; x <= K; ++x) {
    if (x > S)
      continue;

    for (int y = 0; (y <= K); ++y) {
      if (x + y > S)
        continue;

      z = S - x - y;
      if (z >= 0 && z <= K) {
        // cout << x << ":" << y << ":" << z << endl;
        ++ans;
      }
    }
  }

  cout << ans << endl;

  return 0;
}
