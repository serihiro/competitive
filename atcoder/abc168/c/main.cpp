#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

#define PI 3.14159265358979323846264338327950L

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  long double A, B, H, M;
  cin >> A >> B >> H >> M;

  long double rad = PI * 2 * ((H / 12.0) + M / 60.0 / 12.0 - M / 60.0);
  long double ans = (A * A + B * B) - 2 * A * B * cosl(rad);

  cout << setprecision(20) << sqrt(ans) << endl;

  return 0;
}
