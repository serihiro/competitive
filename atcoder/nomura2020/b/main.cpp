#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  string T;
  cin >> T;
  int l = (int)T.length();
  if (l == 1) {
    if (T.at(0) == '?') {
      cout << "D" << endl;
      return 0;
    }
  }

  if (l == 2) {
    if (T == "??" || T == "?D" || T == "P?") {
      T = "PD";
    } else if (T == "?P" || T == "D?") {
      T = "DP";
    }
    cout << T << endl;
    return 0;
  }

  if (l == 3) {
    int i = 1;
    char c1 = T.at(i - 1);
    char c2 = T.at(i);
    char c3 = T.at(i + 1);

    if (c1 == '?') {
      if (c2 == 'D' || c2 == '?') {
        c1 = 'P';
      } else {
        c1 = 'D';
      }
      T.at(i - 1) = c1;
    }

    if (c2 == '?') {
      if (c1 == 'D' && (c3 == 'D' || c3 == '?')) {
        c2 = 'P';
      } else {
        c2 = 'D';
      }
      T.at(i) = c2;
    }

    if (T.at(2) == '?') {
      T.at(2) = 'D';
    }

    cout << T << endl;
    return 0;
  }

  for (int i = 0; i < l - 3; ++i) {
    char c1 = T.at(i);
    char c2 = T.at(i + 1);
    char c3 = T.at(i + 2);
    char c4 = T.at(i + 3);

    if (c1 == '?') {
      if (c2 == 'D' || c2 == '?') {
        c1 = 'P';
      } else {
        c1 = 'D';
      }
      T.at(i) = c1;
    }

    if (c2 == '?') {
      if (c1 == 'D' && (c3 == 'D' || c3 == '?')) {
        c2 = 'P';
      } else {
        c2 = 'D';
      }
      T.at(i + 1) = c2;
    }

    if (c3 == '?') {
      if (c2 == 'D' && (c4 == 'D' || c4 == '?')) {
        c4 = 'P';
      } else {
        c4 = 'D';
      }
      T.at(i + 2) = c4;
    }
  }

  if (T.at(l - 1) == '?') {
    T.at(l - 1) = 'D';
  }

  cout << T << endl;

  return 0;
}