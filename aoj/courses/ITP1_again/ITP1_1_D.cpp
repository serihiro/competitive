#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int S;
  cin >> S;

  int h = S / 3600;
  int m = (S % 3600) / 60;
  int s = S % 60;

  cout << h << ":" << m << ":" << s << endl;

  return 0;
}
