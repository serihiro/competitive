#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, n)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N, M;
  cin >> N >> M;
  int tmp_a, tmp_b;
  vector<vector<char>> R(N, vector<char>(N, '-'));

  for (int i = 0; i < M; ++i) {
    cin >> tmp_a >> tmp_b;
    R.at(tmp_a - 1).at(tmp_b - 1) = 'o';
    R.at(tmp_b - 1).at(tmp_a - 1) = 'x';
  }
  for (int i = 0; i < N; ++i) {
    for (int j = 0; j < N; ++j) {
      cout << R.at(i).at(j);
      if (j != N - 1) {
        cout << ' ';
      }
    }
    cout << endl;
  }

  return 0;
}
