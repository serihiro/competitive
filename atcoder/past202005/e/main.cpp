#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N, M, Q;
  cin >> N >> M >> Q;
  vector<vector<int>> G(N);
  int u, v;
  REP(i, M) {
    cin >> u >> v;
    --u;
    --v;
    G.at(u).push_back(v);
    G.at(v).push_back(u);
  }
  vector<int> color(N);
  REP(i, N) { cin >> color.at(i); }

  int q, x, y;
  REP(i, Q) {
    cin >> q;
    if (q == 1) {
      cin >> x;
      --x;
      cout << color.at(x) << endl;
      for (int const &j : G.at(x)) {
        color.at(j) = color.at(x);
      }
    } else {
      cin >> x >> y;
      --x;
      cout << color.at(x) << endl;
      color.at(x) = y;
    }
  }

  return 0;
}
