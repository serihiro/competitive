#include <iostream>
using namespace std;

int main(){
    cin.tie(0);
    ios::sync_with_stdio(false);

    int x;
    int c = 1;
    while(cin >> x){
        if(x == 0)
            return 0;

        cout << "Case " << c << ": " << x << endl;
        ++c;
    }

    return 0;
}
