#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int H, W;
  cin >> H >> W;
  vector<vector<char>> S(H);
  char s;
  REP(i, H) {
    REP(j, W) {
      cin >> s;
      S.at(i).push_back(s);
    }
  }

  vector<int> K = {-1, 0, 0, 1};
  vector<int> L = {0, -1, 1, 0};

  bool ok = true;
  REP(i, H) {
    REP(j, W) {
      if (S.at(i).at(j) == '#') {
        int found_black = false;
        for (int k = 0; k < 4; ++k) {
          int ny = i + K.at(k);
          int nx = j + L.at(k);
          if ((0 <= ny && ny < H) && (0 <= nx && nx < W)) {
            if (S.at(ny).at(nx) == '#') {
              // cout << i << "/" << j << "|" << ny << ":" << nx << endl;
              found_black = true;
              break;
            }
          }
        }

        if (!found_black) {
          ok = false;
          break;
        }
      }
    }

    if (!ok) {
      break;
    }
  }

  if (ok) {
    cout << "Yes" << endl;
  } else {
    cout << "No" << endl;
  }

  return 0;
}
