// https://beta.atcoder.jp/contests/abc089
// giveup..
// and fixed

#include <iostream>
#include <map>
#include <set>
#include <vector>
using namespace std;

int main()
{
    cin.tie(0);
    ios::sync_with_stdio(false);

    int N;
    string name;

    cin >> N;
    long long a[5] = {};

    for (int i = 0; i < N; ++i)
    {
        cin >> name;
        if (name[0] == 'M')
            ++a[0];
        if (name[0] == 'A')
            ++a[1];
        if (name[0] == 'R')
            ++a[2];
        if (name[0] == 'C')
            ++a[3];
        if (name[0] == 'H')
            ++a[4];
    }

    long long ans = 0, p = 1;
    for (int i = 0; i < 3; i++)
        for (int j = i + 1; j < 4; j++)
            for (int k = j + 1; k < 5; k++)
            {
                p = a[i] * a[j] * a[k];
                ans += p;
            }

    cout << ans << endl;

    return 0;
}
