#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int digit(ll v) {
  int ans = 1;
  while (v > 9) {
    v /= 10;
    ++ans;
  }
  return 0;
}

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  ll A, R, N;
  cin >> A >> R >> N;

  int a_d = digit(A);
  int r_d = digit(R);
  int n_d = digit(N);

  int total_d;
  if (N == 1) {
    total_d = a_d;
  } else {
    total_d = a_d * r_d;
  }

  return 0;
}
