#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, n)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N, S;
  cin >> N >> S;

  vector<int> A(N);
  vector<int> P(N);
  for (int i = 0; i < N; ++i) {
    cin >> A.at(i);
  }
  for (int i = 0; i < N; ++i) {
    cin >> P.at(i);
  }

  int ans = 0;
  for (int i = 0; i < N; ++i) {
    for (int j = 0; j < N; ++j) {
      if (A.at(i) + P.at(j) == S) {
        ++ans;
      }
    }
  }

  cout << ans << endl;

  return 0;
}
