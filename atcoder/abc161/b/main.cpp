#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N, M;
  cin >> N >> M;
  vector<int> A(N);
  int sum = 0;
  REP(i, N) {
    cin >> A.at(i);
    sum += A.at(i);
  }
  int count = 0;
  REP(i, N) {
    if (A.at(i) * 4 * M >= sum) {
      ++count;
    }
  }

  if (count >= M) {
    cout << "Yes" << endl;
  } else {
    cout << "No" << endl;
  }

  return 0;
}
