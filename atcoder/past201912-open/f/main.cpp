#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  string S;
  cin >> S;

  vector<string> s;
  bool reading = false;
  string tmp = "";
  for (char c : S) {
    tmp.push_back(c);
    if ('A' <= c && c <= 'Z') {
      if (reading) {
        s.push_back(tmp);
        tmp = "";
        reading = false;
        continue;
      } else {
        reading = true;
      }
    }
  }

  vector<string> s_c(s.size());
  copy(s.begin(), s.end(), s_c.begin());
  for (string &ts : s_c) {
    transform(ts.begin(), ts.end(), ts.begin(), ::tolower);
  }

  map<string, string> m;
  REP(i, s.size()) {
    if (m.find(s_c.at(i)) != m.end()) {
      m.at(s_c.at(i)) += s.at(i);
    } else {
      m.insert(make_pair(s_c.at(i), s.at(i)));
    }
  }

  for (auto const &it : m) {
    cout << it.second;
  }

  cout << endl;
  return 0;
}
