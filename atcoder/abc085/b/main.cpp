#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i, a, b) for (int i = int(a); i < int(b); ++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  cin >> N;
  if (N == 1) {
    cout << 1 << endl;
    return 0;
  }

  vector<int> d(N);
  REP(i, N) { cin >> d.at(i); }
  sort(d.begin(), d.end());
  int ans = 1;
  int prev_d = d.at(0);
  REPI(i, 1, N) {
    if (d.at(i) > prev_d) {
      ++ans;
    }
    prev_d = d.at(i);
  }

  cout << ans << endl;

  return 0;
}
