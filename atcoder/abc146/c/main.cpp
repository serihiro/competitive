#include <bits/stdc++.h>

#define REP(i, x) for (int i = 0; i < (int)(x); i++)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int digit(ll v) {
  int result = 1;
  while (v > 9) {
    v /= 10;
    ++result;
  }
  return result;
}

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  ll A, B, X;
  cin >> A >> B >> X;
  if (A + B > X) {
    cout << 0 << endl;
    return 0;
  }

  return 0;
}
