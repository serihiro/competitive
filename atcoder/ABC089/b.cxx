// https://beta.atcoder.jp/contests/abc089 

#include <iostream>
#include <set>
using namespace std;

int main()
{
    cin.tie(0);
    ios::sync_with_stdio(false);

    int N = 0;
    int i = 0;
    set<char> H = {};
    char S;
    cin >> N;
    while(i < N && cin >> S){
        H.insert(S);
        if(H.size() == 4){
            cout << "Four" << endl;
            return 0;
        }
        ++i;
    }
    if(H.size() == 3){
         cout << "Three" << endl;
    }

    return 0;
}
