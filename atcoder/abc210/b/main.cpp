#include <bits/stdc++.h>

#define REP(i, x) REPI(i, 0, x)
#define REPI(i,a,b) for(int i=int(a);i<int(b);++i)
#define ALL(x) (x).begin(), (x).end()

typedef long long ll;
using namespace std;

int main() {
  cin.tie(0);
  ios::sync_with_stdio(false);

  int N;
  string S;
  cin >> N >> S;
  string ans = "";
  REP(i, N){
    if(S.at(i) == '1'){
      if(i % 2 == 0) {
        ans = "Takahashi";
      }else{
        ans = "Aoki";
      }
      break;
    }
  }

  cout << ans << endl;

  return 0;
}

